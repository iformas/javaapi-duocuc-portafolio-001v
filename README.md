# JavaApi-DuocUC-Portafolio-001V
El siguiente proyecto está realizado en [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)  utilizando [NetBeans 8.2](https://netbeans.org/downloads)

###  Clonar Proyecto
```sh
git clone git@gitlab.com:iformas/javaapi-duocuc-portafolio-001v.git
```
###  CD/CI
  - El proyecto cuenta con integración continua, cualquier nuevo cambio será deployado en
```sh
  http://development.myloli.moe:8088/AVOT-web/
```

  - El estado de los deplyments puede ser visualizado [aquí](https://gitlab.com/iformas/javaapi-duocuc-portafolio-001v/pipelines)

  El log de payara puede ser visualizado con la ayuda de http-tail (requiere [NodeJS](https://nodejs.org))
```sh
  npm -i -g tail-http
  tail-http http://development.myloli.moe:8090/
```

###  Setup
  - Descomprimir el contenedor de aplicaciones proporcionado (Payara) en C:
  - Instalar los plugins proporcionados para NetBeans accediendo a Tools/Plugins/Downloaded/Add Plugins
  - En NetBeans agregar el conenedor de aplicaciones desde el cuadro de herramientas "Services" (ctrl+5) /Servers/(click Derecho) Add Server/Payara Server seleccionando la carpeta previamente descomprimida
  - Agregar los 4 proyectos a NetBeans (File/Open Projects)
  - Verificar que todos estén configurados para desplegarse sobre el servidor de payara previamente configurado (click derecho sobre el proyecto/Properties/Run/Server)

###  Scaffolding
##### JPA
###### Servicios
-   com.tsurath.entity/Click derecho/New/Other../Persistence/Entity Clases from Database
###### Session Beams
-   com.tsurath.beams/Click derecho/New/Other../Persistence/Session Beams for Entity Clases

###  Email
avotcompany@gmail.com / zsedcx.123.zx