/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils.email;

import java.util.List;

/**
 *
 * @author iformas
 */
 public class EmailContent {

        private String From;
        private List<String> To;
        private String Subject;
        private StringBuilder Content;

        public EmailContent() {
        }

        public EmailContent(String From, List<String> To, String Subject, StringBuilder Content) {
            this.From = From;
            this.To = To;
            this.Subject = Subject;
            this.Content = Content;
        }

        public StringBuilder getContent() {
            return Content;
        }

        public void setContent(StringBuilder Content) {
            this.Content = Content;
        }

        public String getFrom() {
            return From;
        }

        public void setFrom(String From) {
            this.From = From;
        }

        public List<String> getTo() {
            return To;
        }

        public void setTo(List<String> For) {
            this.To = For;
        }

        public String getSubject() {
            return Subject;
        }

        public void setSubject(String Subject) {
            this.Subject = Subject;
        }

    }
