/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils.email;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author iformas
 */
//https://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
public class Email {

    private Protocol protocol;
    private Provider provider;
    private Credentials credentials;
    private EmailContent content;

    public Email(Protocol protocol, Provider provider, Credentials credentials, EmailContent content) {
        this.protocol = protocol;
        this.provider = provider;
        this.credentials = credentials;
        this.content = content;
    }

    public void Send() throws IOException {

        if (provider != null && protocol != null && content != null && credentials != null) {
            switch (protocol) {
                case SSL:
                    Properties propsSSL = new Properties();
                    propsSSL.put("mail.smtp.host", provider.getHost());
                    propsSSL.put("mail.smtp.socketFactory.port", provider.getPort());
                    propsSSL.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                    propsSSL.put("mail.smtp.auth", "true");
                    propsSSL.put("mail.smtp.port", provider.getPort());

                    Session sessionSSL = Session.getInstance(propsSSL,
                            new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(credentials.getUsername(), credentials.getPassword());
                        }
                    });

                    try {

                        Message message = new MimeMessage(sessionSSL);
                        message.setFrom(new InternetAddress(content.getFrom()));
                        message.setRecipients(Message.RecipientType.TO,
                                InternetAddress.parse(String.join(",", content.getTo())));
                        message.setSubject(content.getSubject());
                        message.setContent(content.getContent().toString(), "text/html; charset=utf-8");

                    
                        Transport.send(message);

                        //System.out.println("Done");
                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }

                    break;
                case SMTP:
                    Properties propsSMTP = new Properties();
                    propsSMTP.put("mail.smtp.auth", "true");
                    propsSMTP.put("mail.smtp.starttls.enable", "true");
                    propsSMTP.put("mail.smtp.host", provider.getHost());
                    propsSMTP.put("mail.smtp.port", provider.getPort());

                    Session sessionSMTP = Session.getInstance(propsSMTP,
                            new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(credentials.getUsername(), credentials.getPassword());
                        }
                    });

                    try {

                        Message message = new MimeMessage(sessionSMTP);
                        message.setFrom(new InternetAddress(content.getFrom()));
                        message.setRecipients(Message.RecipientType.TO,
                                InternetAddress.parse(String.join(",", content.getTo())));
                        message.setSubject(content.getSubject());
                        message.setText(content.toString());

                        Transport.send(message);

                        // System.out.println("Done");
                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }

                    break;
            }
        } else {
            throw new IOException("No se han inizailizado todos los parametros");
        }

    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public EmailContent getContent() {
        return content;
    }

    public void setContent(EmailContent content) {
        this.content = content;
    }

}
