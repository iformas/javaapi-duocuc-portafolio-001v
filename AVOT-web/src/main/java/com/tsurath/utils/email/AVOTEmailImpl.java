/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils.email;

import com.tsurath.utils.IConfiguration;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author iformas
 */
public class AVOTEmailImpl implements IEmail, IConfiguration {

    private Protocol protocol;
    private Provider provider;
    private Credentials credentials;

    public AVOTEmailImpl() {
        this.protocol = protocol.SSL;
        this.provider = new Provider("smtp.gmail.com", "465");
        this.credentials = new Credentials("avotcompany@gmail.com", "zsedcx.123.zx");
    }

    @Override
    public void Custom(List<String> toEmail, String content, String subject) throws IOException {
        StringBuilder strBld = new StringBuilder();
        strBld.append(content);
        EmailContent eContent = new EmailContent();
        eContent.setFrom("not-reply@avot.com");
        eContent.setTo(toEmail);
        eContent.setSubject(subject);
        eContent.setContent(CustomTemplate(strBld));
        new Email(protocol, provider, credentials, eContent).Send();
    }

    @Override
    public void Register(String toEmail, String token) throws IOException {
        EmailContent content = new EmailContent();
        content.setFrom("not-reply@avot.com");
        content.setTo(Arrays.asList(toEmail));
        content.setSubject("Email de Confirmacion");
        content.setContent(RegisterTemplate(token));
        new Email(protocol, provider, credentials, content).Send();
    }

    @Override
    public void Voucher(String toEmail, BigDecimal voucherId) throws IOException {
        EmailContent content = new EmailContent();
        content.setFrom("not-reply@avot.com");
        content.setTo(Arrays.asList(toEmail));
        content.setSubject("Comrpobante de pago");
        content.setContent(VoucherTemplate(voucherId));
        new Email(protocol, provider, credentials, content).Send();
    }

    public StringBuilder CustomTemplate(StringBuilder content) {
        StringBuilder sb = new StringBuilder();
        sb.append("						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("							<tr>");
        sb.append("								<td width=\"100%\" colspan=\"3\" align=\"center\" style=\"padding-bottom:10px;padding-top:25px;\">");
        sb.append("									<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='center' >");
        sb.append("					                  		<h2 >Hola!</h2>");
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("							</tr>");
        sb.append("							<tr>");
        sb.append("								<td width=\"100\">&nbsp;</td>");
        sb.append("								<td width=\"400\" align=\"center\">");
        sb.append("									<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='left' >");
        sb.append(content.toString());
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("								<td width=\"100\">&nbsp;</td>");
        sb.append("							</tr>");

        return Template(sb);
    }

    public StringBuilder VoucherTemplate(BigDecimal voucherId) {
        String voucherUrl = API_URL + "private/deposito/voucher/" + voucherId.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("							<tr>");
        sb.append("								<td width=\"100%\" colspan=\"3\" align=\"center\" style=\"padding-bottom:10px;padding-top:25px;\">");
        sb.append("									<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='center' >");
        sb.append("					                  		<h2 >Hola!</h2>");
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("							</tr>");
        sb.append("							<tr>");
        sb.append("								<td width=\"100\">&nbsp;</td>");
        sb.append("								<td width=\"400\" align=\"center\">");
        sb.append("									<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='left' >");
        sb.append("					                  		<p >Hola,");
        sb.append("					                  			<br/>");
        sb.append("					                  			<br/>");
        sb.append("												Haz click en el siguiente botón para obtener el comprobante de tu deposito.Si tu no solisitaste esto, por favor ignora este mensaje.</p>");
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("								<td width=\"100\">&nbsp;</td>");
        sb.append("							</tr>");
        sb.append("						</table>");
        sb.append("						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("							<tr>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("								<td width=\"200\" align=\"center\" style=\"padding-top:25px;\">");
        sb.append("									<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"200\" height=\"50\">");
        sb.append("										<tr>");
        sb.append("											<td bgcolor=\"#ED006F\" align=\"center\" style=\"border-radius:4px;\" width=\"200\" height=\"50\">");
        sb.append("												<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("								                	<div class=\"contentEditable\" align='center' >");
        sb.append("								                  		<a target='_blank' href=\"" + voucherUrl + "\" class='link2'>Descargar comprobante</a>");
        sb.append("								                	</div>");
        sb.append("								              	</div>");
        sb.append("											</td>");
        sb.append("										</tr>");
        sb.append("									</table>");
        sb.append("								</td>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("							</tr>");
        sb.append("						</table>");
        return Template(sb);
    }

    public StringBuilder RegisterTemplate(String token) {
        String confirmationUrl = API_URL + "auth/validate/" + token;
        StringBuilder sb = new StringBuilder();
        sb.append("						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("							<tr>");
        sb.append("								<td width=\"100%\" colspan=\"3\" align=\"center\" style=\"padding-bottom:10px;padding-top:25px;\">");
        sb.append("									<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='center' >");
        sb.append("					                  		<h2 >It's been a while...</h2>");
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("							</tr>");
        sb.append("							<tr>");
        sb.append("								<td width=\"100\">&nbsp;</td>");
        sb.append("								<td width=\"400\" align=\"center\">");
        sb.append("									<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='left' >");
        sb.append("					                  		<p >Hola,");
        sb.append("					                  			<br/>");
        sb.append("					                  			<br/>");
        sb.append("												Haz click en el siguiente botón para activar tu cuenta.Si tu no solisitaste esto, por favor ignora este mensaje.</p>");
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("								<td width=\"100\">&nbsp;</td>");
        sb.append("							</tr>");
        sb.append("						</table>");
        sb.append("						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("							<tr>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("								<td width=\"200\" align=\"center\" style=\"padding-top:25px;\">");
        sb.append("									<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"200\" height=\"50\">");
        sb.append("										<tr>");
        sb.append("											<td bgcolor=\"#ED006F\" align=\"center\" style=\"border-radius:4px;\" width=\"200\" height=\"50\">");
        sb.append("												<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("								                	<div class=\"contentEditable\" align='center' >");
        sb.append("								                  		<a target='_blank' href=\"" + confirmationUrl + "\" class='link2'>Confirmar Email</a>");
        sb.append("								                	</div>");
        sb.append("								              	</div>");
        sb.append("											</td>");
        sb.append("										</tr>");
        sb.append("									</table>");
        sb.append("								</td>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("							</tr>");
        sb.append("						</table>");
        return Template(sb);
    }

    @Override
    public StringBuilder Template(StringBuilder content) {
        StringBuilder sb = new StringBuilder();

        sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
        sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
        sb.append("<head>");
        sb.append("	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
        sb.append("	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>");
        sb.append("	<title>[SUBJECT]</title>");
        sb.append("	<style type=\"text/css\">");
        sb.append("@media screen and (max-width: 600px) {");
        sb.append("    table[class=\"container\"] {");
        sb.append("        width: 95% !important;");
        sb.append("    }");
        sb.append("}");
        sb.append("	#outlook a {padding:0;}");
        sb.append("		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}");
        sb.append("		.ExternalClass {width:100%;}");
        sb.append("		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}");
        sb.append("		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}");
        sb.append("		img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}");
        sb.append("		a img {border:none;}");
        sb.append("		.image_fix {display:block;}");
        sb.append("		p {margin: 1em 0;}");
        sb.append("		h1, h2, h3, h4, h5, h6 {color: black !important;}");
        sb.append("		h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}");
        sb.append("		h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {");
        sb.append("			color: red !important; ");
        sb.append("		 }");
        sb.append("		h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {");
        sb.append("			color: purple !important; ");
        sb.append("		}");
        sb.append("		table td {border-collapse: collapse;}");
        sb.append("		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }");
        sb.append("		a {color: #000;}");
        sb.append("		@media only screen and (max-device-width: 480px) {");
        sb.append("			a[href^=\"tel\"], a[href^=\"sms\"] {");
        sb.append("						text-decoration: none;");
        sb.append("						color: black; /* or whatever your want */");
        sb.append("						pointer-events: none;");
        sb.append("						cursor: default;");
        sb.append("					}");
        sb.append("			.mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {");
        sb.append("						text-decoration: default;");
        sb.append("						color: orange !important; /* or whatever your want */");
        sb.append("						pointer-events: auto;");
        sb.append("						cursor: default;");
        sb.append("					}");
        sb.append("		}");
        sb.append("		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {");
        sb.append("			a[href^=\"tel\"], a[href^=\"sms\"] {");
        sb.append("						text-decoration: none;");
        sb.append("						color: blue; /* or whatever your want */");
        sb.append("						pointer-events: none;");
        sb.append("						cursor: default;");
        sb.append("					}");
        sb.append("			.mobile_link a[href^=\"tel\"], .mobile_link a[href^=\"sms\"] {");
        sb.append("						text-decoration: default;");
        sb.append("						color: orange !important;");
        sb.append("						pointer-events: auto;");
        sb.append("						cursor: default;");
        sb.append("					}");
        sb.append("		}");
        sb.append("		@media only screen and (-webkit-min-device-pixel-ratio: 2) {");
        sb.append("			/* Put your iPhone 4g styles in here */");
        sb.append("		}");
        sb.append("		@media only screen and (-webkit-device-pixel-ratio:.75){");
        sb.append("			/* Put CSS for low density (ldpi) Android layouts in here */");
        sb.append("		}");
        sb.append("		@media only screen and (-webkit-device-pixel-ratio:1){");
        sb.append("			/* Put CSS for medium density (mdpi) Android layouts in here */");
        sb.append("		}");
        sb.append("		@media only screen and (-webkit-device-pixel-ratio:1.5){");
        sb.append("			/* Put CSS for high density (hdpi) Android layouts in here */");
        sb.append("		}");
        sb.append("		/* end Android targeting */");
        sb.append("		h2{");
        sb.append("			color:#181818;");
        sb.append("			font-family:Helvetica, Arial, sans-serif;");
        sb.append("			font-size:22px;");
        sb.append("			line-height: 22px;");
        sb.append("			font-weight: normal;");
        sb.append("		}");
        sb.append("		a.link1{");
        sb.append("		}");
        sb.append("		a.link2{");
        sb.append("			color:#fff;");
        sb.append("			text-decoration:none;");
        sb.append("			font-family:Helvetica, Arial, sans-serif;");
        sb.append("			font-size:16px;");
        sb.append("			color:#fff;border-radius:4px;");
        sb.append("		}");
        sb.append("		p{");
        sb.append("			color:#555;");
        sb.append("			font-family:Helvetica, Arial, sans-serif;");
        sb.append("			font-size:16px;");
        sb.append("			line-height:160%;");
        sb.append("		}");
        sb.append("	</style>");
        sb.append("<script type=\"colorScheme\" class=\"swatch active\">");
        sb.append("  {");
        sb.append("    \"name\":\"Default\",");
        sb.append("    \"bgBody\":\"ffffff\",");
        sb.append("    \"link\":\"fff\",");
        sb.append("    \"color\":\"555555\",");
        sb.append("    \"bgItem\":\"ffffff\",");
        sb.append("    \"title\":\"181818\"");
        sb.append("  }");
        sb.append("</script>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("	<!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->");
        sb.append("	<table cellpadding=\"0\" width=\"100%\" cellspacing=\"0\" border=\"0\" id=\"backgroundTable\" class='bgBody'>");
        sb.append("	<tr>");
        sb.append("		<td>");
        sb.append("	<table cellpadding=\"0\" width=\"620\" class=\"container\" align=\"center\" cellspacing=\"0\" border=\"0\">");
        sb.append("	<tr>");
        sb.append("		<td>");
        sb.append("		<!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->");
        sb.append("		");
        sb.append("		<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("			<tr>");
        sb.append("				<td class='movableContentContainer bgItem'>");
        sb.append("					<div class='movableContent'>");
        sb.append("						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("							<tr height=\"40\">");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("							</tr>");
        sb.append("							<tr>");
        sb.append("								<td width=\"200\" valign=\"top\">&nbsp;</td>");
        sb.append("								<td width=\"200\" valign=\"top\" align=\"center\">");
        sb.append("									<div class=\"contentEditableContainer contentImageEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='center' >");
        sb.append("					                  		<h1>AVOT</h1>");
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("								<td width=\"200\" valign=\"top\">&nbsp;</td>");
        sb.append("							</tr>");
        sb.append("							<tr height=\"25\">");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("								<td width=\"200\">&nbsp;</td>");
        sb.append("							</tr>");
        sb.append("						</table>");
        sb.append("					</div>");
        sb.append("					<div class='movableContent'>");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append(content.toString());
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("                        ");
        sb.append("					</div>");
        sb.append("					<div class='movableContent'>");
        sb.append("						<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" width=\"600\" class=\"container\">");
        sb.append("							<tr>");
        sb.append("								<td width=\"100%\" colspan=\"2\" style=\"padding-top:65px;\">");
        sb.append("									<hr style=\"height:1px;border:none;color:#333;background-color:#ddd;\" />");
        sb.append("								</td>");
        sb.append("							</tr>");
        sb.append("							<tr>");
        sb.append("								<td width=\"60%\" height=\"70\" valign=\"middle\" style=\"padding-bottom:20px;\">");
        sb.append("									<div class=\"contentEditableContainer contentTextEditable\">");
        sb.append("					                	<div class=\"contentEditable\" align='left' >");
        sb.append("					                  		<span style=\"font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;\">Enviado por AVOT</span>");
        sb.append("										");
        sb.append("					                	</div>");
        sb.append("					              	</div>");
        sb.append("								</td>");
        sb.append("								<td width=\"40%\" height=\"70\" align=\"right\" valign=\"top\" align='right' style=\"padding-bottom:20px;\">");
        sb.append("									<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align='right'>");
        sb.append("										<tr>");
        sb.append("							");
        sb.append("										</tr>");
        sb.append("									</table>");
        sb.append("								</td>");
        sb.append("							</tr>");
        sb.append("						</table>");
        sb.append("					</div>");
        sb.append("				</td>");
        sb.append("			</tr>");
        sb.append("		</table>");
        sb.append("	</td></tr></table>");
        sb.append("	");
        sb.append("		</td>");
        sb.append("	</tr>");
        sb.append("	</table>");
        sb.append("</body>");
        sb.append("</html>");

        return sb;

    }

}
