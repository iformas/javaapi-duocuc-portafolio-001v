/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils.email;

/**
 *
 * @author iformas
 */
public class Provider {

        private String host;
        private String port;

        public Provider() {
        }

        public Provider(String host, String port) {
            this.host = host;
            this.port = port;

        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getPort() {
            return port;
        }

        public void setPort(String port) {
            this.port = port;
        }
    }