/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils.email;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author iformas
 */
public interface IEmail {

    public void Custom(List<String> ToEmail, String content, String subject) throws IOException;

    public void Register(String ToEmail, String token) throws IOException;

    public void Voucher(String ToEmail, BigDecimal voucherId) throws IOException;

    public StringBuilder Template(StringBuilder content);
}
