/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils;

import com.tsurath.entity.ArchivoJson;
import com.tsurath.entity.Metadata;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author migue
 */
public class MetadataString extends Metadata {
    private ArchivoJson archivoIdFile;

    public MetadataString() {
        super();
    }

    public MetadataString(BigDecimal id) {
        super(id);
    }

    public MetadataString(BigDecimal id, String nombre, String mime, String hash, Date fecha) {
        super(id, nombre, mime, hash, fecha);
    }

    public ArchivoJson getArchivoIdFile() {
        return archivoIdFile;
    }

    public void setArchivoIdFile(ArchivoJson archivoIdFile) {
        this.archivoIdFile = archivoIdFile;
    }
    
    
}
