/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTClaimsSet.Builder;
import com.nimbusds.jwt.SignedJWT;
import com.tsurath.entity.Perfil;
import com.tsurath.entity.Usuario;
import com.tsurath.models.Token;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.joda.time.DateTime;
import java.net.URL;

/**
 *
 * @author iformas
 */
public final class AuthUtils {

    private static String JWK_RESOURCE_NAME = "/sample.json";
    private static String JWK_RESOURCE_URL = "http://localhost:8080/AVOT-web/JWK.json";
    private static String JWK_KEY_ID = "sample";

    public static String getSubject(String authHeader) throws ParseException, JOSEException, URISyntaxException, IOException {
        return decodeToken(authHeader).getSubject();
    }

    public static JWTClaimsSet decodeToken(String authHeader) throws URISyntaxException, JOSEException, IOException, MalformedURLException, ParseException {
        SignedJWT signedJWT = SignedJWT.parse(authHeader);
        if (signedJWT.verify(new RSASSAVerifier(getPublicKey(JWK_RESOURCE_NAME, JWK_KEY_ID)))) {
            return signedJWT.getJWTClaimsSet();
        } else {
            throw new JOSEException("Signature verification failed");
        }
    }

    public static Token createToken(String host, Usuario user) throws JOSEException, IOException, ParseException, URISyntaxException {
        //https://www.javacodegeeks.com/2016/01/jwt-generating-validating-token-samples.html

        if (user == null) {
            throw new IOException("usuario invalido");
        }

        user.getPerfilId().setUsuarioList(null);
        List<Perfil> perfiles = new ArrayList<>(Arrays.asList(user.getPerfilId()));

        //PRIVATE AND PUBLIC KEYS
        RSAPublicKey publicKey = getPublicKey(JWK_RESOURCE_NAME, JWK_KEY_ID);
        RSAPrivateKey privateKey = getPrivateKey(JWK_RESOURCE_NAME, JWK_KEY_ID);

        System.out.println("-----BEGIN RSA PRIVATE KEY-----");
        System.out.println(Base64.encode(privateKey.getEncoded()));
        System.out.println("-----END RSA PRIVATE KEY-----");
        System.out.println("");
        System.out.println("-----BEGIN PUBLIC KEY-----");
        System.out.println(Base64.encode(publicKey.getEncoded()));
        System.out.println("-----END PUBLIC KEY-----");

        JWTClaimsSet claimsSetBuilder = new Builder()
                .subject(user.getId().toString())
                .claim("user", user.getPersonaId().getNombres() + " " + user.getPersonaId().getAmaterno() + " " + user.getPersonaId().getApaterno())
                .claim("roles", Arrays.toString(perfiles.toArray()))
                .issuer(host)
                .expirationTime(DateTime.now().plusDays(1).toDate()).build();

        RSASSASigner signer = new RSASSASigner(privateKey);
        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.RS512), claimsSetBuilder);
        signedJWT.sign(signer);

        return new Token(signedJWT.serialize());
    }

    public static RSAPublicKey getPublicKey(String jwkResourceName, String keyId) throws URISyntaxException, JOSEException, MalformedURLException, IOException, ParseException {
        return getJWK(jwkResourceName, keyId).toRSAPublicKey();

    }

    public static RSAPrivateKey getPrivateKey(String jwkResourceName, String keyId) throws URISyntaxException, JOSEException, MalformedURLException, IOException, ParseException {
        return getJWK(jwkResourceName, keyId).toRSAPrivateKey();
    }

    private static RSAKey getJWK(String jwkName, String keyId) throws URISyntaxException, IOException, ParseException {

        //URL url = Thread.currentThread().getContextClassLoader().getResource(jwkName).toURI().toURL();
        URL url2 = new URL(JWK_RESOURCE_URL);

        JWKSet jwkSet = JWKSet.load(url2);
        return (RSAKey) jwkSet.getKeyByKeyId(keyId);
    }
}
