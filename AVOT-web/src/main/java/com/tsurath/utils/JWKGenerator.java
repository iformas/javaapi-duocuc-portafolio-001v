/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.utils;

import com.google.gson.Gson;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import com.google.gson.JsonParser;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author iformas
 */
public class JWKGenerator {

    public void make(Integer keySize, KeyUse keyUse, Algorithm keyAlg, String keyId) throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(keySize);
        KeyPair kp = generator.generateKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) kp.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) kp.getPrivate();
        new RSAKey.Builder(publicKey)
                .privateKey(privateKey)
                .keyUse(keyUse)
                .algorithm(keyAlg)
                .keyID(keyId)
                .build();
    }

    /**
     * Generate a JWK Key Pair from a RSA key
     *
     * @param rsaKey
     * @return
     */
    public JsonElement generateJWKKeypair(RSAKey rsaKey) {
        JWKSet jwkSet = new JWKSet(rsaKey);
        return new JsonParser().parse(jwkSet.toJSONObject(false).toJSONString());
    }

    /**
     * Generate a JWK json from a RSA Key
     *
     * @param rsaKey
     * @return
     */
    public String generateJWKJson(RSAKey rsaKey) {
        JsonElement jsonElement = generateJWKKeypair(rsaKey);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(jsonElement);

    }
}
