/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.filter;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import com.tsurath.entity.Perfil;
import com.tsurath.entity.Usuario;
import com.tsurath.utils.AuthUtils;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.Principal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.ejb.EJB;
import javax.ws.rs.Priorities;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import org.joda.time.DateTime;

/**
 *
 * @author iformas
 */
//https://www.programcreek.com/java-api-examples/?code=maugern/jersey-skeleton/jersey-skeleton-master/src/main/java/fr/maugern/skeleton/auth/AuthFilter.java#
@Provider
@PreMatching
@Priority(Priorities.AUTHENTICATION)
public class RESTRequestFilter implements ContainerRequestFilter {

    
    @EJB
    private com.tsurath.beams.UsuarioFacadeLocal usuarioFacade;
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException, MalformedURLException {
        if (requestContext.getMethod().equals("OPTIONS")) {
            //  throw new WebApplicationException(Status.OK);
            Response response = Response.status(Response.Status.OK)
                    .header("Access-Control-Allow-Methods", "OPTIONS, GET, POST, DELETE, PUT")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Credentials, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With, observe")
                    .build();
            requestContext.abortWith(response);
        } else {          
            requestContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
            requestContext.getHeaders().add("Access-Control-Allow-Origin", "*");
            requestContext.getHeaders().add("Access-Control-Allow-Methods", "OPTIONS, GET, POST, DELETE, PUT");
            requestContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Credentials, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With, observe");

        }
        
        
          try {

            SecurityContext originalCOntext = requestContext.getSecurityContext();
            String authHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
            if (authHeader == null || authHeader.isEmpty() || authHeader.split(" ").length != 2) {
                Authorizer authorizer = new Authorizer(new ArrayList<Perfil>(), new User(), originalCOntext.isSecure());
                requestContext.setSecurityContext(authorizer);
            } else {
                JWTClaimsSet claimSet;
                try {
                    claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader.split(" ")[1]);
                } catch (ParseException e) {
                    throw new IOException("Error al decodificar JWT");
                } catch (JOSEException e) {
                    throw new IOException("Token invalido");
                }

                if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
                    throw new IOException("El token ya expiro");
                } else {
                    Usuario usuario = usuarioFacade.find(new BigDecimal(claimSet.getSubject()));
                    User user = new User(usuario.getId(), usuario.getEmail(), usuario.getPassword());

                    Authorizer authorizer = new Authorizer(new ArrayList<>(Arrays.asList(usuario.getPerfilId())), user, originalCOntext.isSecure());

                    requestContext.setSecurityContext(authorizer);
                }
            }
        } catch (URISyntaxException ex) {
            Logger.getLogger(RESTRequestFilter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public static class Authorizer implements SecurityContext {

        List<Perfil> roles;
        User user;
        boolean isSecure;

        public Authorizer(List<Perfil> roles, User user, boolean isSecure) {
            this.roles = roles;
            this.user = user;
            this.isSecure = isSecure;
        }

        @Override
        public Principal getUserPrincipal() {
            //return new SecurityFilter.User(username);
            return this.user;
        }

        @Override
        public boolean isUserInRole(String role) {
            for (int i = 0; i < roles.size(); i++) {
                if (roles.get(i).getNombre().equals(role)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean isSecure() {
            return true;
            //return isSecure;
            // return "https".equals(this.scheme);
        }

        @Override
        public String getAuthenticationScheme() {
            return SecurityContext.BASIC_AUTH;
        }

    }

    public static class User implements Principal {

        private BigDecimal id;
        private String name;
        private String password;

        public User() {
        }

        public User(BigDecimal id, String name, String password) {
            this.id = id;
            this.name = name;
            this.password = password;
        }

        @Override
        public String getName() {
            return this.name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

    }
    
}
