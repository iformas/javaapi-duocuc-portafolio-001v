/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.filter;

import java.io.IOException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author iformas
 */
@Provider
@PreMatching
public class RESTResponseFilter implements ContainerResponseFilter {

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        MultivaluedMap<String, Object> headers = responseContext.getHeaders();      
     
        if (responseContext.getHeaderString("Access-Control-Allow-Origin") == null || "".equals(responseContext.getHeaderString("Access-Control-Allow-Origin"))) {
            responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
        }
        if (responseContext.getHeaderString("Access-Control-Allow-Credentials") == null || "".equals(responseContext.getHeaderString("Access-Control-Allow-Credentials"))) {
            responseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
        }
        if (responseContext.getHeaderString("Access-Control-Allow-Methods") == null || "".equals(responseContext.getHeaderString("Access-Control-Allow-Methods"))) {
            responseContext.getHeaders().add("Access-Control-Allow-Methods", "OPTIONS, GET, POST, DELETE, PUT");
        }
        if (responseContext.getHeaderString("Access-Control-Allow-Origin") == null || "".equals(responseContext.getHeaderString("Access-Control-Allow-Origin"))) {
               responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
        }
        if (responseContext.getHeaderString("Access-Control-Allow-Headers") == null || "".equals(responseContext.getHeaderString("Access-Control-Allow-Headers"))) {
              responseContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Origin, Access-Control-Allow-Credentials, Access-Control-Allow-Methods, Access-Control-Allow-Headers, Authorization, X-Requested-With, observe");
        }
    }

}
