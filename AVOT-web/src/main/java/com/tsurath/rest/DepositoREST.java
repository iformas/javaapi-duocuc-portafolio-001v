package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Actividad;
import com.tsurath.entity.Alumno;
import com.tsurath.entity.Contrato;
import com.tsurath.entity.Deposito;
import com.tsurath.entity.Usuario;
import com.tsurath.models.Result;
import com.tsurath.utils.DateUtil;
import com.tsurath.utils.email.AVOTEmailImpl;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/deposito")

public class DepositoREST {

    @EJB
    private com.tsurath.beams.DepositoFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.ActividadFacadeLocal actividadFacade;
    @EJB
    private com.tsurath.beams.AlumnoFacadeLocal alumnoFacade;
    @EJB
    private com.tsurath.beams.ContratoFacadeLocal contratoFacade;
    @EJB
    private com.tsurath.beams.UsuarioFacadeLocal usuarioFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Deposito> elems = entityFacade.findAll();
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listByDay/{date}")
    public String ListByDay(@PathParam("date") String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            String dateInString = date +" 00:00:00";
            Date dateParsed = sdf.parse(dateInString);

            List<Deposito> elems = entityFacade.findAll().stream().filter(f -> DateUtil.getDateWithoutTimeUsingFormat(f.getFecha()).equals(dateParsed)).collect(Collectors.toList());
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Deposito elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findByActividad/{id}")
    public String findByActividad(@PathParam("id") BigDecimal id) {
        try {
            List<Deposito> elem = entityFacade.findByActividad(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getTotalRecaudado/{idContrato}")
    public String GetTotalRecaudado(@PathParam("idContrato") int idContrato) {
        try {
            return Serializer().toJson(Result.Ok(entityFacade.getTotalRecaudado(idContrato)));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Deposito elem) {
        try {

            Actividad actividad = (elem.getActividadId() != null && elem.getActividadId().getId() != null) ? actividadFacade.find(elem.getActividadId().getId()) : null;
            Alumno alumno = (elem.getAlumnoId() != null && elem.getAlumnoId().getId() != null) ? alumnoFacade.find(elem.getAlumnoId().getId()) : null;
            elem.setId(null);
            Contrato contrato = contratoFacade.find(elem.getContratoId().getId());
            Usuario usuario = usuarioFacade.find(elem.getUsuarioId().getId());

            if (usuario == null) {
                throw new Exception("Usuario no existe. no se guardará el deposito");
            } else {
                elem.setUsuarioId(usuario);
            }
            if (alumno == null && actividad == null) {
                throw new Exception("Debe ingresar una actividad o un alumno para crear un deposito. no se guardará el deposito");
            } else if (alumno != null) {
                elem.setAlumnoId(alumno);
            } else if (actividad != null) {
                elem.setActividadId(actividad);
            }
            if (contrato == null) {
                throw new Exception("Contrato no existe. no se guardará el deposito");
            } else {
                elem.setContratoId(contrato);
            }
            String emailEncargado = "";
            if (contrato.getCursoId().getEncargadoCurso() != null) {
                emailEncargado = contrato.getCursoId().getEncargadoCurso().getEmail();
            }
            elem.setFecha(new Date());
            Deposito entity = entityFacade.create(elem);
            AVOTEmailImpl email = new AVOTEmailImpl();
            email.Custom( Arrays.asList(usuario.getEmail(),emailEncargado), "Ha realizado un deposito de " + entity.getMonto() + " a " + (alumno != null ? "el alumno " + alumno.getPersonaId().getNombres() : "de la actividad " + actividad.getNombre()), "Comprobante de Deposito");
            return new Gson().toJson(Result.Ok(entity.getId()));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/createDepositoActividad")
    public String CreateDepositoActividad(Deposito elem) {
        try {
            elem.getActividadId().setEliminado(BigInteger.ZERO);
            elem.setEstado("0");
            Actividad actividad = (elem.getActividadId() != null) ? actividadFacade.create(elem.getActividadId()) : null;
            elem.setId(null);
            Contrato contrato = contratoFacade.find(elem.getContratoId().getId());
            Usuario usuario = usuarioFacade.find(elem.getUsuarioId().getId());
            if (actividad == null) {
                throw new Exception("No ha ingresado los datos de la actividad, no se guardará el deposito");
            }
            if (usuario == null) {
                throw new Exception("Usuario no existe. no se guardará el deposito");
            } else {
                elem.setUsuarioId(usuario);
            }
            
            if (contrato == null) {
                throw new Exception("Contrato no existe. no se guardará el deposito");
            } else {
                elem.setContratoId(contrato);
            }
            elem.setFecha(new Date());
            Deposito entity = entityFacade.create(elem);
            return new Gson().toJson(Result.Ok(entity.getId()));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Deposito elem) {
        try {
            Deposito entity = entityFacade.find(elem.getId());
            entity.setActividadId((elem.getActividadId() != null && elem.getActividadId().getId() != null) ? actividadFacade.find(elem.getActividadId().getId()) : null);
            entity.setAlumnoId((elem.getAlumnoId() != null && elem.getAlumnoId().getId() != null) ? alumnoFacade.find(elem.getAlumnoId().getId()) : null);
            entity.setContratoId((elem.getContratoId() != null && elem.getContratoId().getId() != null) ? contratoFacade.find(elem.getContratoId().getId()) : null);
            entity.setEstado(elem.getEstado());
            entity.setFecha(elem.getFecha());
            entity.setLlaveTransaccion(elem.getLlaveTransaccion());
            entity.setMonto(elem.getMonto());
            entity.setUsuarioId((elem.getUsuarioId() != null && elem.getUsuarioId().getId() != null) ? usuarioFacade.find(elem.getUsuarioId().getId()) : null);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            Deposito entity = entityFacade.find(id);
            entity.setEliminado(BigInteger.ONE);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        return (ActividadREST.SkipField(f)
                || AlumnoREST.SkipField(f)
                || UsuarioREST.SkipField(f)
                || ContratoREST.SkipField(f)
                || UsuarioREST.SkipField(f));
    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
