/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//https://www.youtube.com/watch?v=5lQgi-n05F4
//https://github.com/swagger-api/swagger-core/wiki/swagger-core-resteasy-2.x-project-setup-1.5#using-the-application-class
package com.tsurath.rest;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

/**
 *
 * @author iformas
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        packages("com.tsurath.filter;com.tsurath.rest");
        register(RolesAllowedDynamicFeature.class);

        property("jersey.config.server.tracing.type", "ALL");
        property("jersey.config.server.tracing.threshold", "VERBOSE");
    }
}
