package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.beams.UsuarioFacade;
import com.tsurath.entity.Alumno;
import com.tsurath.entity.Contrato;
import com.tsurath.entity.Curso;
import com.tsurath.entity.Usuario;
import com.tsurath.models.Result;
import com.tsurath.models.SaldoPorAlumnoYCurso;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/alumno")

public class AlumnoREST {

    @EJB
    private com.tsurath.beams.AlumnoFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.PersonaFacadeLocal personaFacade;
    @EJB
    private com.tsurath.beams.CursoFacadeLocal cursoFacade;
    @EJB
    private com.tsurath.beams.UsuarioFacadeLocal usuarioFacade;
    @EJB
    private com.tsurath.beams.ContratoFacadeLocal contratoFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Alumno> elems = entityFacade.findAll();
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Alumno elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findByRutYCurso/{rut}/{dv}/{cursoId}")
    public String FindById(@PathParam("rut") BigInteger rut, @PathParam("dv") char dv, @PathParam("dv") BigDecimal cursoId) {
        try {
            Alumno elem = entityFacade.findPorRutYCurso(rut, dv, cursoId);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Alumno elem) {
        try {
            Alumno entity = entityFacade.create(elem);
            return new Gson().toJson(Result.Ok(entity.getId()));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Alumno elem) {
        try {
            Alumno entity = entityFacade.find(elem.getId());
            entity.setCursoId((elem.getCursoId() != null && elem.getCursoId().getId() != null) ? cursoFacade.find(elem.getCursoId().getId()) : null);
            entity.setPersonaId((elem.getPersonaId() != null && elem.getPersonaId().getId() != null) ? personaFacade.find(elem.getPersonaId().getId()) : null);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            Alumno entity = entityFacade.find(id);
            entity.setEliminado(BigInteger.ONE);
            //entity.getCursoId().setId(null);
            //Curso entityTemp = new Curso();
            entity.setCursoId(null);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listPorApoderado/{id}")
    public String ListPorApoderado(@PathParam("id") BigDecimal id) {
        try {
            List<Alumno> elems = entityFacade.ListPorApoderado(id);
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listPorCurso/{id}")
    public String ListPorCurso(@PathParam("id") BigDecimal id) {
        try {
            List<Alumno> elems = entityFacade.ListPorCurso(id);
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listPorColegio/{id}")
    public String ListPorColegio(@PathParam("id") BigDecimal id) {
        try {
            List<Alumno> elems = entityFacade.ListPorColegio(id);
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/agregarApoderado")
    public String AgregarApoderado(Alumno elem) {
        try {
            Alumno entity = entityFacade.find(elem.getId());
            if (elem.getUsuarioList() != null && elem.getUsuarioList().size() > 0) {
                for (Usuario usuarioList : elem.getUsuarioList()) {
                    Usuario apoderado = usuarioFacade.find(usuarioList.getId());
                    if ((apoderado.getPerfilId().getId().compareTo(new BigDecimal("4")) == 0 || apoderado.getPerfilId().getId().compareTo(new BigDecimal("5")) == 0) && apoderado.getEliminado().compareTo(BigInteger.ZERO) == 0) {
                        //ok
                    } else {
                        return new Gson().toJson(Result.NoOk(Arrays.asList("El usuario no existe o no posee el perfil de apoderado ni encargado de curso")));
                    }

                }

                for (Usuario usuarioList : elem.getUsuarioList()) {
                    entity.getUsuarioList().add(usuarioFacade.find(usuarioList.getId()));
                    entityFacade.edit(entity);
                }
            }

            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/saldoPorAlumnoYContrato/{idContrato}/{idAlumno}")
    public String saldoPorAlumnoYContrato(@PathParam("idContrato") BigDecimal idContrato, @PathParam("idAlumno") BigDecimal idAlumno) {
        try {
            BigDecimal saldo = entityFacade.SaldoAlumno(idContrato, idAlumno);
            if ("-1".equals(saldo.toString())) {
                throw new Exception("Alumno y/o contrato no Existe");
            } else {
                return Serializer().toJson(Result.Ok(saldo));
            }

        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/saldoAlumno/{idAlumno}")
    public String SaldoAlumno(@PathParam("idAlumno") BigDecimal idAlumno) {
        try {
            Alumno al = entityFacade.find(idAlumno);
            List<Contrato> contratos = contratoFacade.listByCurso(entityFacade.find(idAlumno).getCursoId().getId());
            List<SaldoPorAlumnoYCurso> saldosPorAlumno = new ArrayList<>();
            for (Contrato contrato : contratos) {
                BigDecimal saldo = entityFacade.SaldoAlumno(contrato.getId(), idAlumno);
                if ("-1".equals(saldo.toString())) {
                    throw new Exception("Alumno y/o contrato no Existe");
                }
                saldosPorAlumno.add(new SaldoPorAlumnoYCurso(contrato, saldo));
            }
            return SaldoPorAlumnoYCurso.Serializer().toJson(Result.Ok(saldosPorAlumno));

        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/montoDepositadoAlumno/{idContrato}/{idAlumno}")
    public String MontoDepositadoAlumno(@PathParam("idContrato") BigDecimal idContrato, @PathParam("idAlumno") BigDecimal idAlumno) {
        try {
            BigDecimal saldo = entityFacade.MontoDepositadoAlumno(idContrato, idAlumno);
            if ("-1".equals(saldo.toString())) {
                throw new Exception("Alumno y/o contrato no Existe");
            } else {
                return Serializer().toJson(Result.Ok(saldo));
            }
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/eliminarApoderado")
    public String EliminarApoderado(Alumno elem) {
        try {
            Alumno entity = entityFacade.find(elem.getId());
            if (elem.getUsuarioList() != null && elem.getUsuarioList().size() > 0) {
                for (Usuario usuarioList : elem.getUsuarioList()) {
                    Usuario apoderado = usuarioFacade.find(usuarioList.getId());
                }

                for (Usuario usuarioList : elem.getUsuarioList()) {
                    entity.getUsuarioList().remove(usuarioFacade.find(usuarioList.getId()));
                    entityFacade.edit(entity);
                }
            }

            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Alumno.class && f.getName().equals("depositoList")
                || f.getDeclaringClass() == Alumno.class && f.getName().equals("personaList")
                || f.getDeclaringClass() == Alumno.class && f.getName().equals("usuarioList")
                || PersonaREST.SkipField(f)
                || CursoREST.SkipField(f)); 
    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
