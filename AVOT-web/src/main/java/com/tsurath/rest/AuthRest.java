package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nimbusds.jose.JOSEException;
import com.tsurath.beams.UsuarioFacadeLocal;
import com.tsurath.entity.Usuario;
import com.tsurath.models.Auth;
import com.tsurath.models.Login;
import com.tsurath.models.Result;
import com.tsurath.models.Token;
import com.tsurath.utils.AuthUtils;
import com.tsurath.rest.UsuarioREST;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Arrays;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 *
 * @author iformas
 */
@Stateless
@Path("auth")

public class AuthRest {

    public static final String CLIENT_ID_KEY = "client_id", REDIRECT_URI_KEY = "redirect_uri",
            CLIENT_SECRET = "client_secret", CODE_KEY = "code", GRANT_TYPE_KEY = "grant_type",
            AUTH_CODE = "authorization_code";

    public static final String NOT_FOUND_MSG = "User not found", LOGING_ERROR_MSG = "Wrong email and/or password";

    @EJB
    private UsuarioFacadeLocal entityFacade;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login/")
    public String login(@FormParam("email") String email, @FormParam("password") String password) {
        try {
            Usuario result = entityFacade.login(email, password);

            return Serializer().toJson(Result.Ok(new Auth("", !(result == null), result)));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    //
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/loginElectron/")
    public Response login(Login login) {
        try {
            Usuario result = entityFacade.login(login.getEmail(), login.getPassword());
            if (result != null && result.getPerfilId().getId().equals(new BigDecimal("3"))) {
                return Response.ok(Serializer().toJson(Result.Ok(new Auth("", !(result == null), result))), MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(401).build();
            }
        } catch (Exception e) {
            return Response.status(401).build();
        }
    }

    @POST
    @Path("login2")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response login2(Usuario user, @Context final HttpServletRequest request) throws JOSEException, IOException, ParseException, URISyntaxException {

        final Usuario foundUser;
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        foundUser = entityFacade.findByEmail(user.getEmail());
        if (foundUser == null) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(gson.toJson(NOT_FOUND_MSG)).build();
        } else if (user.getPassword() != null && foundUser.getPassword() != null && user.getPassword().equals(foundUser.getPassword())) {
            final Token token = AuthUtils.createToken(request.getRemoteHost(), foundUser);
            return Response.ok().entity(gson.toJson(token)).build();
        }
        return Response.status(Response.Status.UNAUTHORIZED).entity(gson.toJson(LOGING_ERROR_MSG)).build();

    }

    public static boolean SkipField(FieldAttributes f) {
        return (UsuarioREST.SkipField(f));
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/changePassword/")
    public String changePassword(@FormParam("email") String username, @FormParam("oldPassword") String oldPassword, @FormParam("newPassword") String newPassword) {
        try {
            Usuario result = entityFacade.login(username, oldPassword);
            if (result != null) {
                Usuario usua = entityFacade.findByEmail(username);
                usua.setPassword(newPassword);
                entityFacade.edit(usua);
                return Serializer().toJson(Result.Ok(new Auth("", !(result == null), result)));
            }
            return new Gson().toJson(Result.NoOk(Arrays.asList("Credenciales Incorrectas")));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }
}
