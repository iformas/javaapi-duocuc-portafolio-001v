package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Metadata;
import com.tsurath.entity.Archivo;
import com.tsurath.entity.ArchivoJson;
import com.tsurath.entity.Contrato;
import com.tsurath.models.Result;
import com.tsurath.entity.MetadataString;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.wink.common.model.multipart.BufferedInMultiPart;
import org.apache.wink.common.model.multipart.InPart;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/metadata")

public class MetadataREST {

    @EJB
    private com.tsurath.beams.MetadataFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.ArchivoFacadeLocal archivoFacadeLocal;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Metadata> elems = entityFacade.findAll();
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Metadata elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/create")
    public String Create(Metadata elem) {
        try {
            Archivo arch = null;
            Metadata entity = null;
            if (elem.getArchivoId() != null && elem.getArchivoId().getId() == null) {
                arch = archivoFacadeLocal.create(elem.getArchivoId());
            }
            try {
                entity = entityFacade.create(elem);
            } catch (Exception e) {
                if (arch != null) {
                    archivoFacadeLocal.remove(arch);
                }
                throw e;
            }

            return new Gson().toJson(Result.Ok(entity.getId()));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Metadata elem) {
        try {
            Metadata entity = entityFacade.find(elem.getId());
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            entityFacade.remove(entityFacade.find(id));
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Metadata.class && f.getName().equals("contratoList")
                || f.getDeclaringClass() == Metadata.class && f.getName().equals("publicacionList")
                || f.getDeclaringClass() == Metadata.class && f.getName().equals("respuestaList")
                || f.getDeclaringClass() == Metadata.class && f.getName().equals("servicioList")
                || ArchivoREST.SkipField(f));

    }

    public static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
