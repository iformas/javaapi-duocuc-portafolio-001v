package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Perfil;
import com.tsurath.entity.Persona;
import com.tsurath.entity.Usuario;
import com.tsurath.models.Result;
import com.tsurath.models.EjecutivoVentas;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/usuario")

public class UsuarioREST {
    
    @EJB
    private com.tsurath.beams.UsuarioFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.PersonaFacadeLocal personaFacade;
    @EJB
    private com.tsurath.beams.PerfilFacadeLocal perfilFacade;
    
    @Context
    SecurityContext securityContext;
    
    @GET
    // @RolesAllowed({"Administrador"})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Usuario> elems = entityFacade.findAll();
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Usuario elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Usuario elem) {
        try {
            
            Persona p = null;
            Usuario entity = null;
            if (elem.getPersonaId() != null && elem.getPersonaId().getId() == null) {
               p =  personaFacade.create(elem.getPersonaId()); 
               elem.setPersonaId(personaFacade.find(p.getId()));
            }            
            try {
                entity = entityFacade.create(elem);
            } catch (Exception e) {
                if (p!= null) personaFacade.remove(p);
                throw e;
            }
            
            //if (!securityContext.isUserInRole("Administrador")) {
            //     return new Gson().toJson(Result.NoOk(Arrays.asList("Permisos insificientes para asignar el rol especificado")));
            // }
            //Se crea verificaition token
            //se envia email con token de verificacion
            return new Gson().toJson(Result.Ok(entity.getId()));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Usuario elem) {
        try {
            Usuario entity = entityFacade.find(elem.getId());
            entity.setEmail(elem.getEmail());
            entity.setPerfilId((elem.getPerfilId() != null && elem.getPerfilId().getId() != null) ? perfilFacade.find(elem.getPerfilId().getId()) : null);
            entity.setPersonaId((elem.getPersonaId() != null && elem.getPersonaId().getId() != null) ? personaFacade.find(elem.getPersonaId().getId()) : null);
            
            if (elem.getPerfilId() != null && elem.getPerfilId().getId() != null) {
                Perfil perfil = perfilFacade.find(elem.getPerfilId().getId());
                if (!(perfil.getId().compareTo(new BigDecimal("4")) == 0) || !(perfil.getId().compareTo(new BigDecimal("5")) == 0)) {
                    entity.setAlumnoList(null);
                }
            }
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            Usuario entity = entityFacade.find(id);
            entity.setEliminado(BigInteger.ONE);
            
            if (entity.getAlumnoList().size() > 0) {
                entity.setAlumnoList(null);
            }
            
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @GET
    // @RolesAllowed({"Administrador"})
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listEjecutivoVentas")
    public String ListEjecutivoVentas() {
        try {
            List<EjecutivoVentas> ejecutivosVenta = new ArrayList<>();
            List<Usuario> elems = entityFacade.findAll().stream().filter(f -> String.valueOf(f.getPerfilId().getId()).equals("2")).collect(Collectors.toList());
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            for (Usuario elem : elems) {
                EjecutivoVentas e = new EjecutivoVentas(elem.getId(), elem.getPersonaId().getNombres(), elem.getPersonaId().getAmaterno(), elem.getPersonaId().getApaterno(), elem.getPassword(), elem.getPersonaId().getDireccion(), elem.getPersonaId().getTelefono(), elem.getEliminado(), elem.getPersonaId().getRut(), elem.getPersonaId().getDv(), elem.getEmail());
                ejecutivosVenta.add(e);
            }
            return Serializer().toJson(Result.Ok(ejecutivosVenta));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/createEjecutivoVentas")
    public String CreateVendedor(EjecutivoVentas elem) {
        try {
            Persona p = new Persona(BigInteger.ZERO, elem.getRut(), elem.getDv(), null, elem.getNombres(), elem.getaMaterno(), elem.getaPaterno(), elem.getDireccion(), elem.getTelefono());
            p = personaFacade.create(p);
            
            Usuario entity = entityFacade.create(new Usuario(null, elem.getEmail(), BigInteger.ZERO, null, elem.getPassword(), null, null, perfilFacade.find(new BigDecimal("2")), p));
            try {
                entityFacade.create(entity);
            } catch (Exception e) {
                personaFacade.remove(p);
                throw e;
            }            
            EjecutivoVentas e = new EjecutivoVentas(elem.getId(), entity.getPersonaId().getNombres(), entity.getPersonaId().getAmaterno(), entity.getPersonaId().getApaterno(), elem.getPassword(), entity.getPersonaId().getDireccion(), entity.getPersonaId().getTelefono(), elem.getEliminado(), entity.getPersonaId().getRut(), entity.getPersonaId().getDv(), elem.getEmail());
            return  EjecutivoVentas.Serializer().toJson(Result.Ok(e));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findByRut/{rut}")
    public String FindByRut(@PathParam("rut") BigInteger rut) {
        try {
            Usuario entity = entityFacade.findByRut(rut);
            return Serializer().toJson(Result.Ok(entity));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findByRutAndPerfil/{rut}/{idPerfil}")
    public String FindByRutAndPerfil(@PathParam("rut") BigInteger rut, @PathParam("idPerfil") BigDecimal idPerfil) {
        try {
            Usuario entity = entityFacade.findByRut(rut);
            Perfil perfil = perfilFacade.find(idPerfil);
            
            if (entity.getPerfilId().getId().equals(idPerfil)) {
                return Serializer().toJson(Result.Ok(entity));
            } else {
                return Serializer().toJson(Result.NoOk(new Exception("Usuario no es " + perfil.getNombre())));
            }
            
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/editEjecutivoVentas")
    public String Edit(EjecutivoVentas elem) {
        try {
            Usuario entity = entityFacade.find(elem.getId());
            entity.setEmail(elem.getEmail());
            entity.setPassword(elem.getPassword());
            entity.getPersonaId().setRut(elem.getRut());
            entity.getPersonaId().setDv(elem.getDv());
            entity.getPersonaId().setNombres(elem.getNombres());
            entity.getPersonaId().setAmaterno(elem.getaMaterno());
            entity.getPersonaId().setApaterno(elem.getaPaterno());
            entity.getPersonaId().setDireccion(elem.getDireccion());
            entity.getPersonaId().setTelefono(elem.getTelefono());
            entityFacade.edit(entity);
            
            EjecutivoVentas e = new EjecutivoVentas(elem.getId(), entity.getPersonaId().getNombres(), entity.getPersonaId().getAmaterno(), entity.getPersonaId().getApaterno(), elem.getPassword(), entity.getPersonaId().getDireccion(), entity.getPersonaId().getTelefono(), elem.getEliminado(), entity.getPersonaId().getRut(), entity.getPersonaId().getDv(), elem.getEmail());
            return  EjecutivoVentas.Serializer().toJson(Result.Ok(e));
            
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    
    public static boolean SkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Usuario.class && f.getName().equals("alumnoList")
                || f.getDeclaringClass() == Usuario.class && f.getName().equals("cursoList")
                || f.getDeclaringClass() == Usuario.class && f.getName().equals("contratoList")
                || f.getDeclaringClass() == Usuario.class && f.getName().equals("depositoList")
                || f.getDeclaringClass() == Usuario.class && f.getName().equals("respuestaList")
                || f.getDeclaringClass() == Usuario.class && f.getName().equals("publicacionList")
                || PersonaREST.SkipField(f)
                || PerfilREST.SkipField(f));
    }
    
    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }
                    
                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;
        
    }
    
}
