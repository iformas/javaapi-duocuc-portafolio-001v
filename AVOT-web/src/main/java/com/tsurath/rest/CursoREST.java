package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Alumno;
import com.tsurath.entity.Colegio;
import com.tsurath.entity.Curso;
import com.tsurath.entity.Usuario;
import com.tsurath.models.Result;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/curso")

public class CursoREST {

    @EJB
    private com.tsurath.beams.CursoFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.UsuarioFacadeLocal usuarioFacade;
    @EJB
    private com.tsurath.beams.ColegioFacadeLocal colegioFacade;
    @EJB
    private com.tsurath.beams.AlumnoFacadeLocal alumnoFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Curso> elems = entityFacade.findAll();
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listEncargado/{id}")
    public String ListPorEncargado(@PathParam("id") BigDecimal id) {
        try {
            List<Curso> elems = entityFacade.listPorEncargado(id);
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list/{id}")
    public String ListPorColegio(@PathParam("id") BigDecimal id) {
        try {
            List<Curso> elems = entityFacade.listPorColegio(id);
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Curso elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findByContrato/{id}")
    public String FindByContratoId(@PathParam("id") BigDecimal id) {
        try {
            Curso elem = entityFacade.findByContrato(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Curso elem) {
        try {
            Colegio colegio = colegioFacade.find(elem.getColegioId().getId());
            if (colegio != null) {
                elem.setColegioId(colegio);
                Usuario encargadoCurso = usuarioFacade.find(elem.getEncargadoCurso().getId());
                encargadoCurso.setAlumnoList(null);
                encargadoCurso.setCursoList(null);
                if (encargadoCurso != null) {
                    elem.setEncargadoCurso(encargadoCurso);
                    Curso entity = entityFacade.create(elem);
                    //entity.getColegioId().setCursoList(null);
                    //entity.getEncargadoCurso().setAlumnoList(null);
                    //entity.getEncargadoCurso().setCursoList(null);
                    //entity.setActividadList(null);
                    return new Gson().toJson(Result.Ok(entity.getId()));
                } else {
                    throw new Exception("Encargado de curso no existe, favor agregarlo.");
                }
            } else {
                throw new Exception("el colegio no existe, favor agregarlo.");
            }

        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Curso elem) {
        try {
            Curso entity = entityFacade.find(elem.getId());
            entity.setColegioId((elem.getColegioId() != null && elem.getColegioId().getId() != null) ? colegioFacade.find(elem.getColegioId().getId()) : null);
            entity.setEncargadoCurso((elem.getEncargadoCurso() != null && elem.getEncargadoCurso().getId() != null) ? usuarioFacade.find(elem.getEncargadoCurso().getId()) : null);
            entity.setNombre(elem.getNombre());
            entity.setAnho(elem.getAnho());
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            Curso entity = entityFacade.find(id);
            entity.setEliminado(BigInteger.ONE);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/agregarAlumno")
    public String AgregarAlumno(Alumno elem) {
        try {
            Curso entity = entityFacade.find(elem.getCursoId().getId());
            entity.getAlumnoList().add(alumnoFacade.find(entity.getId()));
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(entity));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/eliminarAlumno")
    public String eliminarAlumno(Alumno elem) {
        try {
            Curso entity = entityFacade.find(elem.getCursoId().getId());
            entity.getAlumnoList().remove(alumnoFacade.find(entity.getId()));
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(entity));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Curso.class && f.getName().equals("actividadList")
                || f.getDeclaringClass() == Curso.class && f.getName().equals("alumnoList")
                || f.getDeclaringClass() == Curso.class && f.getName().equals("contratoList")
                || f.getDeclaringClass() == Curso.class && f.getName().equals("cursoList")
                || UsuarioREST.SkipField(f)
                || ColegioREST.SkipField(f));
    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
