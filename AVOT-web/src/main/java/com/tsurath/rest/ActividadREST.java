package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Actividad;
import com.tsurath.entity.Actividades;
import com.tsurath.models.Result;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/actividad")

public class ActividadREST {

    @EJB
    private com.tsurath.beams.ActividadFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.CursoFacadeLocal cursoFacade;
    @EJB
    private com.tsurath.beams.TipoActividadFacadeLocal tipoActividadFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Actividad> elems = entityFacade.findAll();
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Actividad elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findActivityByContratoID/{idContrato}")
    public String findByContratoID(@PathParam("idContrato") BigDecimal idContrato){
        try {
            List<Actividades> listado = entityFacade.findByContratoID(idContrato);
            if (listado.isEmpty()) {
                throw  new Exception("No hay actividades para el contrato.");
            }
            return Serializer().toJson(Result.Ok(listado));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Actividad elem) {
        try {
            Actividad entity = entityFacade.create(elem);
            return Serializer().toJson(Result.Ok(entity.getId()));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Actividad elem) {
        try {
            Actividad entity = entityFacade.find(elem.getId());
            entity.setCursoId((elem.getCursoId() != null && elem.getCursoId().getId() != null) ? cursoFacade.find(elem.getCursoId().getId()) : null);
            entity.setDescripcion(elem.getDescripcion());
            entity.setFecha(elem.getFecha());
            entity.setNombre(elem.getNombre());
            entity.setTipoActividadId((elem.getTipoActividadId() != null && elem.getTipoActividadId().getId() != null) ? tipoActividadFacade.find(elem.getTipoActividadId().getId()) : null);
            entityFacade.edit(entity);
            return Serializer().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            Actividad entity = entityFacade.find(id);
            entity.setEliminado(BigInteger.ONE);
            entityFacade.edit(entity);
            return Serializer().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        Actividad a = new Actividad();
        return (f.getDeclaringClass() == Actividad.class && f.getName().equals("depositoList")
                || f.getDeclaringClass() == Actividad.class && f.getName().equals("depositoList")
                || CursoREST.SkipField(f)
                || TipoActividadREST.SkipField(f));
    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
