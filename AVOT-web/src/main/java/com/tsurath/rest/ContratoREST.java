package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Contrato;
import com.tsurath.entity.Curso;
import com.tsurath.entity.Metadata;
import com.tsurath.entity.Servicio;
import com.tsurath.entity.Usuario;
import com.tsurath.models.Result;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/contrato")

public class ContratoREST {

    @EJB
    private com.tsurath.beams.ContratoFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.UsuarioFacadeLocal usuarioFacadeLocal;
    @EJB
    private com.tsurath.beams.CursoFacadeLocal cursoFacade;
    @EJB
    private com.tsurath.beams.ServicioFacadeLocal servicioFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Contrato> elems = entityFacade.findAll();
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Contrato elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listByEjecutivo/{id}")
    public String ListByEjecutivo(@PathParam("id") BigDecimal id) {
        try {
            List<Contrato> listContrato = entityFacade.findByEjecutivo(id);
            if (listContrato.size() == 0) {
                List<String> listado = new ArrayList<>();
                listado.add("Usuario no ha creado contratos.");
                return new Gson().toJson(Result.NoOk(listado));
            }
            return Serializer().toJson(Result.Ok(listContrato));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listByCurso/{id}")
    public String ListByCurso(@PathParam("id") BigDecimal id) {
        try {
            List<Contrato> elem = entityFacade.listByCurso(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Contrato elem) {
        try {
            Usuario ejecutivoVentas = usuarioFacadeLocal.find(elem.getEjecutivoVentasId().getId());
            if (ejecutivoVentas != null) {
                elem.setEjecutivoVentasId(ejecutivoVentas);
                Curso curso = cursoFacade.find(elem.getCursoId().getId());
                if (curso != null) {
                    elem.setCursoId(curso);
                    Set<Servicio> listado = new HashSet<>();
                    for (Servicio servicio : elem.getServicioList()) {
                        listado.add(servicioFacade.find(servicio.getId()));
                    }
                    elem.setServicioList(listado);
                    Contrato entity = entityFacade.create(elem);
                    return new Gson().toJson(Result.Ok(entity.getId()));
                } else {
                    throw new Exception("curso no existe.");
                }
            } else {
                throw new Exception("Ejecutivo no existe.");
            }

        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Contrato elem) {
        try {
            Contrato entity = entityFacade.find(elem.getId());
            entity.setCostoBase(elem.getCostoBase());
            entity.setCostoFinal(elem.getCostoFinal());
            entity.setCursoId((elem.getCursoId() != null && elem.getCursoId().getId() != null) ? cursoFacade.find(elem.getCursoId().getId()) : null);
            entity.setDescripcion(elem.getDescripcion());
            entity.setDestino(elem.getDestino());
            entity.setEjecutivoVentasId((elem.getEjecutivoVentasId() != null && elem.getEjecutivoVentasId().getId() != null) ? usuarioFacadeLocal.find(elem.getEjecutivoVentasId().getId()) : null);
            entity.setFechaCreacion(elem.getFechaCreacion());
            entity.setFechaRecaudacionLimite(elem.getFechaRecaudacionLimite());
            entity.setFechaViaje(elem.getFechaViaje());

            if (elem.getEstado().compareTo(new BigInteger("4")) == -1 && elem.getEstado().compareTo(new BigInteger("-1")) == 1) {
                entity.setEstado(elem.getEstado());
            } else {
                return new Gson().toJson(Result.NoOk(Arrays.asList("Estado Invalido")));
            }

            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getMontoFaltante/{idContrato}")
    public String getMontoFaltante(@PathParam("idContrato") BigDecimal idContrato) {
        try {
            BigDecimal monto = entityFacade.getMontoFaltante(idContrato);
            if ("-1".equals(monto.toString())) {
                throw new Exception("Contrato no Existe");
            } else {
                return Serializer().toJson(Result.Ok(monto));
            }
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Path("/buscarMetadataContrato/{idContrato}")
    public javax.ws.rs.core.Response BuscarMetadataContrato(@PathParam("idContrato") BigDecimal idContrato) {
        try {
            Contrato contrato = entityFacade.find(idContrato);
            if (contrato.getMetadataList().isEmpty()) {
                throw new Exception("No existe PDF para este contrato");
            }

            Metadata meta = contrato.getMetadataList().stream().findFirst().get();
            ByteArrayOutputStream baos = new ByteArrayOutputStream(meta.getArchivoId().getData().length);
            baos.write(meta.getArchivoId().getData(), 0, meta.getArchivoId().getData().length);
            ByteArrayInputStream fileInputStream = new ByteArrayInputStream(baos.toByteArray());
            javax.ws.rs.core.Response.ResponseBuilder responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);
            responseBuilder.type(meta.getMime());
            responseBuilder.header("Content-Disposition", "filename="+ meta.getNombre());
            return responseBuilder.build();
            //return contrato.getMetadataList();
        } catch (Exception e) {
            //return new Gson().toJson(Result.NoOk(e));
            return null;
        }
    }

    @POST
    @Produces("application/json")
    @Path("/cambiarEstado")
    public String CambiarEstado(Contrato elem) {
        try {
            Contrato entity = entityFacade.find(elem.getId());
            if (elem.getEstado().compareTo(new BigInteger("4")) == -1 && elem.getEstado().compareTo(new BigInteger("-1")) == 1) {
                entity.setEstado(elem.getEstado());
                entityFacade.edit(entity);
                return new Gson().toJson(Result.Ok(null));

            }
            return new Gson().toJson(Result.NoOk(Arrays.asList("Estado Invalido")));

        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        Contrato cont = new Contrato();

        return (f.getDeclaringClass() == Contrato.class && f.getName().equals("depositoList")
                || f.getDeclaringClass() == Contrato.class && f.getName().equals("metadataList")
                || f.getDeclaringClass() == Contrato.class && f.getName().equals("servicioList")
                || CursoREST.SkipField(f)
                || UsuarioREST.SkipField(f));
    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
