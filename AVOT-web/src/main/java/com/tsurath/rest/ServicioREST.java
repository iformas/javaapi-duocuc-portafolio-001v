package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Servicio;
import com.tsurath.models.Result;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/servicio")

public class ServicioREST {

    @EJB
    private com.tsurath.beams.ServicioFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.TipoServicioFacadeLocal tipoServicioFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Servicio> elems = entityFacade.findAll();
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/find/{id}")
    public String FindById(@PathParam("id") BigDecimal id) {
        try {
            Servicio elem = entityFacade.find(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findByContrato/{id}")
    public String findByContrato(@PathParam("id") BigDecimal id) {
        try {
            List<Servicio> elem = entityFacade.findByContrato(id);
            return Serializer().toJson(Result.Ok(elem));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Servicio elem) {
        try {
            Servicio entity = entityFacade.create(elem);
            return new Gson().toJson(Result.Ok(entity));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Servicio elem) {
        try {
            Servicio entity = entityFacade.find(elem.getId());
            entity.setCosto(elem.getCosto());
            entity.setDescripcion(elem.getDescripcion());
            entity.setNombre(elem.getNombre());
            entity.setTipoServicioId((elem.getTipoServicioId() != null && elem.getTipoServicioId().getId() != null) ? tipoServicioFacade.find(elem.getTipoServicioId().getId()) : null);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            Servicio entity = entityFacade.find(id);
            entity.setEliminado(BigInteger.ONE);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Servicio.class && f.getName().equals("metadataList")
                || f.getDeclaringClass() == Servicio.class && f.getName().equals("contratoList")
                || TipoServicioREST.SkipField(f));

    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
