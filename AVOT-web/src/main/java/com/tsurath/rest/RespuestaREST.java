package com.tsurath.rest;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Publicacion;
import com.tsurath.entity.Respuesta;
import com.tsurath.models.Result;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author iformas
 */
@Stateless
@Path("private/respuesta")

public class RespuestaREST {

    @EJB
    private com.tsurath.beams.RespuestaFacadeLocal entityFacade;
    @EJB
    private com.tsurath.beams.UsuarioFacadeLocal usuarioFacade;
    @EJB
    private com.tsurath.beams.PublicacionFacadeLocal publicacionFacade;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public String List() {
        try {
            List<Respuesta> elems = entityFacade.findAll();
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list/{id}")
    public String ListPorPublicacion(@PathParam("id") BigDecimal id) {
        try {
            List<Respuesta> elems = entityFacade.listPorPublicacion(id);
            elems.removeIf(p -> p.getEliminado().compareTo(BigInteger.ZERO) == 1);
            return Serializer().toJson(Result.Ok(elems));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/create")
    public String Create(Respuesta elem) {
        try {
            Respuesta entity = entityFacade.create(elem);
            return new Gson().toJson(Result.Ok(entity));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/edit")
    public String Edit(Respuesta elem) {
        try {
            Respuesta entity = entityFacade.find(elem.getId());
            entity.setContenido(elem.getContenido());
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    @POST
    @Produces("application/json")
    @Path("/delete/{id}")
    public String Delete(@PathParam("id") BigDecimal id) {
        try {
            Respuesta entity = entityFacade.find(id);
            entity.setEliminado(BigInteger.ONE);
            entityFacade.edit(entity);
            return new Gson().toJson(Result.Ok(null));
        } catch (Exception e) {
            return new Gson().toJson(Result.NoOk(e));
        }
    }

    public static boolean SkipField(FieldAttributes f) {
        return (f.getDeclaringClass() == Respuesta.class && f.getName().equals("metadataList")
                || f.getDeclaringClass() == Respuesta.class && f.getName().equals("publicacionList")
                || UsuarioREST.SkipField(f));
    }

    private static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
