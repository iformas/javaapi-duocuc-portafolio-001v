/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.models;

import com.tsurath.entity.Usuario;

/**
 *
 * @author iformas
 */
public class Auth {

    private String Token;
    private boolean Result;
    private Usuario Usuario;
    

    public Auth() {
    }

    public Auth(boolean Result) {
        this.Result = Result;
    }

    public Auth(String Token) {
        this.Token = Token;
    }

    public Auth(String Token, boolean Result, Usuario Usuario) {
        this.Token = Token;
        this.Result = Result;
        this.Usuario = Usuario;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

}
