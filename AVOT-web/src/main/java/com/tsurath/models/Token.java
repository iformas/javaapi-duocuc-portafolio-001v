/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.models;

/**
 *
 * @author iformas
 */
public class Token {

    String token;

    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}