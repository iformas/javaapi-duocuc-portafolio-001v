/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.models;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.rest.ContratoREST;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author iformas
 */
public class EjecutivoVentas {

    private BigDecimal id;
    private String nombres;
    private String aMaterno;
    private String aPaterno;
    private String password;
    private String direccion;
    private String telefono;
    private BigInteger eliminado;
    private BigInteger rut;
    private Character dv;
    private String email;

    public EjecutivoVentas() {
    }

    public EjecutivoVentas(BigDecimal id, String nombres, String aMaterno, String aPaterno, String password, String direccion, String telefono, BigInteger eliminado, BigInteger rut, Character dv, String email) {
        this.id = id;
        this.nombres = nombres;
        this.aMaterno = aMaterno;
        this.aPaterno = aPaterno;
        this.password = password;
        this.direccion = direccion;
        this.telefono = telefono;
        this.eliminado = eliminado;
        this.rut = rut;
        this.dv = dv;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public BigInteger getEliminado() {
        return eliminado;
    }

    public void setEliminado(BigInteger eliminado) {
        this.eliminado = eliminado;
    }

    public BigInteger getRut() {
        return rut;
    }

    public void setRut(BigInteger rut) {
        this.rut = rut;
    }

    public Character getDv() {
        return dv;
    }

    public void setDv(Character dv) {
        this.dv = dv;
    }

    public static boolean SkipField(FieldAttributes f) {
        return (false);
    }

    public static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }

}
