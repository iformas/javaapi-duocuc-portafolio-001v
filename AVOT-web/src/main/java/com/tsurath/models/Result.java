/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.models;

import com.tsurath.utils.IConfiguration;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author iformas
 */
public class Result implements IConfiguration {

    private String status;
    private List<String> messages;
    private Object result;

    public Result(String status, List<String> messages, Object result) {
        this.status = status;
        this.messages = messages;
        this.result = result;
    }

    public static Result Ok(Object result) {
        return new Result(Status.OK, null, result);

    }

    public static Result Ok(Object result, List<String> messages) {
        return new Result(Status.OK, messages, result);

    }

    public static Result NoOk(Exception e, List<String> messages) {
        if (DEBUG) {
            messages.add("Cause:" + e.getCause());
            messages.add("Message:" + e.getMessage());
        }

        return new Result(Status.NOOK, messages, null);
    }

    public static Result NoOk(Exception e) {
        List<String> m = new ArrayList<>();
        if (DEBUG) {
            m.add("Cause:" + e.getCause());
            m.add("Message:" + e.getMessage());
        }

        return new Result(Status.NOOK, m, null);
    }

    public static Result NoOk(List<String> messages) {
        List<String> m = new ArrayList<>();
        if (DEBUG) {
            m.addAll(messages);
        }

        return new Result(Status.NOOK, m, null);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<String> messages) {
        this.messages = messages;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
