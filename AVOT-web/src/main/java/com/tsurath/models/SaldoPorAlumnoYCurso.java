/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.models;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tsurath.entity.Alumno;
import com.tsurath.entity.Contrato;
import com.tsurath.rest.ContratoREST;
import com.tsurath.rest.CursoREST;
import com.tsurath.rest.PersonaREST;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author iformas
 */
public class SaldoPorAlumnoYCurso {
    private Contrato contratoId;
    private BigDecimal saldo;

    public SaldoPorAlumnoYCurso(Contrato contratoId, BigDecimal saldo) {
        this.contratoId = contratoId;
        this.saldo = saldo;
    }
    
    

    public Contrato getContratoId() {
        return contratoId;
    }

    public void setContratoId(Contrato contratoId) {
        this.contratoId = contratoId;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }
    
     public static boolean SkipField(FieldAttributes f) {
        return ( ContratoREST.SkipField(f));
    }

    public static Gson Serializer() {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return SkipField(f);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
        return gson;

    }
    
    
}
