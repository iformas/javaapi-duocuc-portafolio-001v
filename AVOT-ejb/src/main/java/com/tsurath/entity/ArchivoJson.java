/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author migue
 */
public class ArchivoJson extends Archivo{

    public ArchivoJson() {
        super();
    }

    public ArchivoJson(BigDecimal id) {
        super(id);
    }

    public ArchivoJson(BigDecimal id, byte[] data) {
        super(id, data);
    }
    
    private String dataArray;

    public String getDataArray() {
        return dataArray;
    }

    public void setDataArray(String dataArray) {
        this.dataArray = dataArray;
        byte[] decodedString;
        try {
            decodedString = Base64.getDecoder().decode(dataArray.getBytes("UTF-8"));
            this.setData(decodedString);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ArchivoJson.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
