/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iformas
 */
@Entity
@Table(name = "SERVICIO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Servicio.findAll", query = "SELECT s FROM Servicio s")
    , @NamedQuery(name = "Servicio.findById", query = "SELECT s FROM Servicio s WHERE s.id = :id")
    , @NamedQuery(name = "Servicio.findByNombre", query = "SELECT s FROM Servicio s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Servicio.findByDescripcion", query = "SELECT s FROM Servicio s WHERE s.descripcion = :descripcion")
    , @NamedQuery(name = "Servicio.findByCosto", query = "SELECT s FROM Servicio s WHERE s.costo = :costo")
    , @NamedQuery(name = "Servicio.findByContrato", query = "SELECT s FROM Servicio s inner join s.contratoList cl WHERE cl.id = :id")

})
public class Servicio implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ELIMINADO")
    private BigInteger eliminado;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "secuencia_servicio", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COSTO")
    private BigInteger costo;
    
     
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "SERVICIOS_CONTRATO", joinColumns = {
        @JoinColumn(name = "SERVICIO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "CONTRATO_ID", referencedColumnName = "ID")})
    private Set<Contrato> contratoList = new HashSet<>();
    
//    @ManyToMany(mappedBy = "servicioList")
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "ARCHIVOS_SERVICIO", joinColumns = {
        @JoinColumn(name = "METADATA_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "SERVICIO_ID", referencedColumnName = "ID")})    
    private Set<Metadata> metadataList = new HashSet<>();
    
    @JoinColumn(name = "TIPO_SERVICIO_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private TipoServicio tipoServicioId;

    public Servicio() {
    }

    public Servicio(BigDecimal id) {
        this.id = id;
    }

    public Servicio(BigDecimal id, String nombre, String descripcion, BigInteger costo) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.costo = costo;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigInteger getCosto() {
        return costo;
    }

    public void setCosto(BigInteger costo) {
        this.costo = costo;
    }

    @XmlTransient
    public Set<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(Set<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @XmlTransient
    public Set<Metadata> getMetadataList() {
        return metadataList;
    }

    public void setMetadataList(Set<Metadata> metadataList) {
        this.metadataList = metadataList;
    }

    public TipoServicio getTipoServicioId() {
        return tipoServicioId;
    }

    public void setTipoServicioId(TipoServicio tipoServicioId) {
        this.tipoServicioId = tipoServicioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Servicio)) {
            return false;
        }
        Servicio other = (Servicio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsurath.entity.Servicio[ id=" + id + " ]";
    }

    public BigInteger getEliminado() {
        return eliminado;
    }

    public void setEliminado(BigInteger eliminado) {
        this.eliminado = eliminado;
    }

}
