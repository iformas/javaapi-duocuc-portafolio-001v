/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iformas
 */
@Entity
@Table(name = "RESPUESTA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Respuesta.findAll", query = "SELECT r FROM Respuesta r")
    , @NamedQuery(name = "Respuesta.findById", query = "SELECT r FROM Respuesta r WHERE r.id = :id")
    , @NamedQuery(name = "Respuesta.findByFecha", query = "SELECT r FROM Respuesta r WHERE r.fecha = :fecha")
    , @NamedQuery(name = "Respuesta.findByContenido", query = "SELECT r FROM Respuesta r WHERE r.contenido = :contenido")

})
public class Respuesta implements Serializable {

    @JoinColumn(name = "PUBLICACION_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Publicacion publicacionId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ELIMINADO")
    private BigInteger eliminado;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "secuencia_respuesta", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "CONTENIDO")
    private String contenido;
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "ARCHIVOS_RESPUESTA", joinColumns = {
        @JoinColumn(name = "RESPUESTA_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "METADATA_ID", referencedColumnName = "ID")})    
    private Set<Metadata> metadataList = new HashSet<>();

    @JoinColumn(name = "USUARIO_ID", referencedColumnName = "ID")
    @ManyToOne
    private Usuario usuarioId;

    public Respuesta() {
    }

    public Respuesta(BigDecimal id) {
        this.id = id;
    }

    public Respuesta(BigDecimal id, Date fecha, String contenido) {
        this.id = id;
        this.fecha = fecha;
        this.contenido = contenido;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    @XmlTransient
    public Set<Metadata> getMetadataList() {
        return metadataList;
    }

    public void setMetadataList(Set<Metadata> metadataList) {
        this.metadataList = metadataList;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Respuesta)) {
            return false;
        }
        Respuesta other = (Respuesta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsurath.entity.Respuesta[ id=" + id + " ]";
    }

    public BigInteger getEliminado() {
        return eliminado;
    }

    public void setEliminado(BigInteger eliminado) {
        this.eliminado = eliminado;
    }

    public Publicacion getPublicacionId() {
        return publicacionId;
    }

    public void setPublicacionId(Publicacion publicacionId) {
        this.publicacionId = publicacionId;
    }

}
