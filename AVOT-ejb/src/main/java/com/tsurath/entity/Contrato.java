/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iformas
 */
@Entity
@Table(name = "CONTRATO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrato.findAll", query = "SELECT c FROM Contrato c")
    , @NamedQuery(name = "Contrato.findById", query = "SELECT c FROM Contrato c WHERE c.id = :id")
    , @NamedQuery(name = "Contrato.findByFechaRecaudacionLimite", query = "SELECT c FROM Contrato c WHERE c.fechaRecaudacionLimite = :fechaRecaudacionLimite")
    , @NamedQuery(name = "Contrato.findByFechaCreacion", query = "SELECT c FROM Contrato c WHERE c.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "Contrato.findByFechaViaje", query = "SELECT c FROM Contrato c WHERE c.fechaViaje = :fechaViaje")
    , @NamedQuery(name = "Contrato.findByCostoBase", query = "SELECT c FROM Contrato c WHERE c.costoBase = :costoBase")
    , @NamedQuery(name = "Contrato.findByCostoFinal", query = "SELECT c FROM Contrato c WHERE c.costoFinal = :costoFinal")
    , @NamedQuery(name = "Contrato.findByDescripcion", query = "SELECT c FROM Contrato c WHERE c.descripcion = :descripcion")
    , @NamedQuery(name = "Contrato.findByDestino", query = "SELECT c FROM Contrato c WHERE c.destino = :destino")
    , @NamedQuery(name = "Contrato.findByCurso", query = "SELECT c FROM Contrato c WHERE c.cursoId.id = :id")
    , @NamedQuery(name = "Contrato.findByEjecutivo", query = "SELECT c FROM Contrato c WHERE c.ejecutivoVentasId.id = :id ORDER BY c.id")}
)
@NamedNativeQueries({
    @NamedNativeQuery(name = "Contrato.getMontoFaltante", query = "SELECT ONTOUR_MONTO_FALTANTE(:idContrato) FROM DUAL")
})
public class Contrato implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ESTADO")
    private BigInteger estado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INICIO_CONTRATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicioContrato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_TERMINO_CONTRATO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaTerminoContrato;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "secuencia_contrato", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_RECAUDACION_LIMITE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecaudacionLimite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_VIAJE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaViaje;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COSTO_BASE")
    private BigInteger costoBase;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COSTO_FINAL")
    private BigInteger costoFinal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "DESTINO")
    private String destino;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "ARCHIVOS_CONTRATO", joinColumns = {
        @JoinColumn(name = "CONTRATO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "METADATA_ID", referencedColumnName = "ID")})
    private Set<Metadata> metadataList = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL) 
    @JoinTable(name = "SERVICIOS_CONTRATO", joinColumns = {
        @JoinColumn(name = "CONTRATO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "SERVICIO_ID", referencedColumnName = "ID")})
    private Set<Servicio> servicioList = new HashSet<>();

    @JoinColumn(name = "CURSO_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Curso cursoId;
    @JoinColumn(name = "EJECUTIVO_VENTAS_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Usuario ejecutivoVentasId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "contratoId")
    private List<Deposito> depositoList;

    public Contrato() {
    }

    public Contrato(BigDecimal id) {
        this.id = id;
    }

    public Contrato(BigDecimal id, Date fechaRecaudacionLimite, Date fechaCreacion, Date fechaViaje, BigInteger costoBase, BigInteger costoFinal, String descripcion, String destino) {
        this.id = id;
        this.fechaRecaudacionLimite = fechaRecaudacionLimite;
        this.fechaCreacion = fechaCreacion;
        this.fechaViaje = fechaViaje;
        this.costoBase = costoBase;
        this.costoFinal = costoFinal;
        this.descripcion = descripcion;
        this.destino = destino;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getFechaRecaudacionLimite() {
        return fechaRecaudacionLimite;
    }

    public void setFechaRecaudacionLimite(Date fechaRecaudacionLimite) {
        this.fechaRecaudacionLimite = fechaRecaudacionLimite;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaViaje() {
        return fechaViaje;
    }

    public void setFechaViaje(Date fechaViaje) {
        this.fechaViaje = fechaViaje;
    }

    public BigInteger getCostoBase() {
        return costoBase;
    }

    public void setCostoBase(BigInteger costoBase) {
        this.costoBase = costoBase;
    }

    public BigInteger getCostoFinal() {
        return costoFinal;
    }

    public void setCostoFinal(BigInteger costoFinal) {
        this.costoFinal = costoFinal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    @XmlTransient
    public Set<Metadata> getMetadataList() {
        return metadataList;
    }

    public void setMetadataList(Set<Metadata> metadataList) {
        this.metadataList = metadataList;
    }

    @XmlTransient
    public Set<Servicio> getServicioList() {
        return servicioList;
    }

    public void setServicioList(Set<Servicio> servicioList) {
        this.servicioList = servicioList;
    }

    public Curso getCursoId() {
        return cursoId;
    }

    public void setCursoId(Curso cursoId) {
        this.cursoId = cursoId;
    }

    public Usuario getEjecutivoVentasId() {
        return ejecutivoVentasId;
    }

    public void setEjecutivoVentasId(Usuario ejecutivoVentasId) {
        this.ejecutivoVentasId = ejecutivoVentasId;
    }

    @XmlTransient
    public List<Deposito> getDepositoList() {
        return depositoList;
    }

    public void setDepositoList(List<Deposito> depositoList) {
        this.depositoList = depositoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrato)) {
            return false;
        }
        Contrato other = (Contrato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsurath.entity.Contrato[ id=" + id + " ]";
    }

    public BigInteger getEstado() {
        return estado;
    }

    public void setEstado(BigInteger estado) {
        this.estado = estado;
    }

    public Date getFechaInicioContrato() {
        return fechaInicioContrato;
    }

    public void setFechaInicioContrato(Date fechaInicioContrato) {
        this.fechaInicioContrato = fechaInicioContrato;
    }

    public Date getFechaTerminoContrato() {
        return fechaTerminoContrato;
    }

    public void setFechaTerminoContrato(Date fechaTerminoContrato) {
        this.fechaTerminoContrato = fechaTerminoContrato;
    }

}
