/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author iformas
 */
@Entity
@Table(name = "DEPOSITO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Deposito.findAll", query = "SELECT d FROM Deposito d")
    , @NamedQuery(name = "Deposito.findById", query = "SELECT d FROM Deposito d WHERE d.id = :id")
    , @NamedQuery(name = "Deposito.findByFecha", query = "SELECT d FROM Deposito d WHERE d.fecha = :fecha")
    , @NamedQuery(name = "Deposito.findByMonto", query = "SELECT d FROM Deposito d WHERE d.monto = :monto")
    , @NamedQuery(name = "Deposito.findByEstado", query = "SELECT d FROM Deposito d WHERE d.estado = :estado")
    , @NamedQuery(name = "Deposito.findByLlaveTransaccion", query = "SELECT d FROM Deposito d WHERE d.llaveTransaccion = :llaveTransaccion")
    , @NamedQuery(name = "Deposito.findByActividad", query = "SELECT d FROM Deposito d inner join d.actividadId a WHERE a.id = :id")
//, @NamedQuery(name = "Deposito.getTotalRecaudado", query = "SELECT ONTOUR_TOTAL_RECAUDADO(:idContrato) FROM DUAL")
})
@NamedNativeQueries({
    @NamedNativeQuery(name = "Deposito.getTotalRecaudado", query = "SELECT ONTOUR_TOTAL_RECAUDADO(:idContrato) FROM DUAL")
})
public class Deposito implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ELIMINADO")
    private BigInteger eliminado;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "secuencia_deposito", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MONTO")
    private BigInteger monto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ESTADO")
    private String estado;
    @Size(max = 30)
    @Column(name = "LLAVE_TRANSACCION")
    private String llaveTransaccion;
    @JoinColumn(name = "ACTIVIDAD_ID", referencedColumnName = "ID")
    @ManyToOne
    private Actividad actividadId;
    @JoinColumn(name = "ALUMNO_ID", referencedColumnName = "ID")
    @ManyToOne
    private Alumno alumnoId;
    @JoinColumn(name = "CONTRATO_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Contrato contratoId;
    @JoinColumn(name = "USUARIO_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Usuario usuarioId;

    public Deposito() {
    }

    public Deposito(BigDecimal id) {
        this.id = id;
    }

    public Deposito(BigDecimal id, Date fecha, BigInteger monto, String estado) {
        this.id = id;
        this.fecha = fecha;
        this.monto = monto;
        this.estado = estado;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getMonto() {
        return monto;
    }

    public void setMonto(BigInteger monto) {
        this.monto = monto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getLlaveTransaccion() {
        return llaveTransaccion;
    }

    public void setLlaveTransaccion(String llaveTransaccion) {
        this.llaveTransaccion = llaveTransaccion;
    }

    public Actividad getActividadId() {
        return actividadId;
    }

    public void setActividadId(Actividad actividadId) {
        this.actividadId = actividadId;
    }

    public Alumno getAlumnoId() {
        return alumnoId;
    }

    public void setAlumnoId(Alumno alumnoId) {
        this.alumnoId = alumnoId;
    }

    public Contrato getContratoId() {
        return contratoId;
    }

    public void setContratoId(Contrato contratoId) {
        this.contratoId = contratoId;
    }

    public Usuario getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Usuario usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Deposito)) {
            return false;
        }
        Deposito other = (Deposito) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsurath.entity.Deposito[ id=" + id + " ]";
    }

    public BigInteger getEliminado() {
        return eliminado;
    }

    public void setEliminado(BigInteger eliminado) {
        this.eliminado = eliminado;
    }

}
