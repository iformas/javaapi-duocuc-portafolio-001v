/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iformas
 */
@Entity
@Table(name = "ALUMNO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alumno.findAll", query = "SELECT a FROM Alumno a")
    , @NamedQuery(name = "Alumno.findById", query = "SELECT a FROM Alumno a WHERE a.id = :id")
    , @NamedQuery(name = "Alumno.findByApoderado", query = "SELECT a FROM Alumno a inner join a.usuarioList ul WHERE ul.id = :id")
    , @NamedQuery(name = "Alumno.findByCurso", query = "SELECT a FROM Alumno a inner join a.cursoId c WHERE c.id = :id")
    , @NamedQuery(name = "Alumno.findByColegio", query = "SELECT a FROM Alumno a inner join a.cursoId c inner join c.colegioId co  WHERE co.id = :id")
    , @NamedQuery(name = "Alumno.findByRutYCurso", query = "SELECT a FROM Alumno a inner join a.cursoId c inner join a.personaId p  WHERE p.rut = :rut AND p.dv = :dv AND c.id = :cursoId")
})
@NamedNativeQueries({
    @NamedNativeQuery(name="Alumno.getSaldoAlumno",query = "SELECT ONTOUR_SALDO_X_ALUMNO(:idContrato, :idAlumno) FROM DUAL"),
    @NamedNativeQuery(name="Alumno.getMontoDepositadoAlumno",query = "SELECT ONTOUR_MONTO_X_ALUMNO(:idContrato, :idAlumno) FROM DUAL")
})
public class Alumno implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ELIMINADO")
    private BigInteger eliminado;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "secuencia_alumno", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
    @Column(name = "ID")
    private BigDecimal id;
    @JoinTable(name = "ALUMNOS_USUARIO", joinColumns = {
        @JoinColumn(name = "ALUMNO_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "USUARIO_ID", referencedColumnName = "ID")})
    @ManyToMany
    private List<Usuario> usuarioList;
    @JoinColumn(name = "CURSO_ID", referencedColumnName = "ID")
    @ManyToOne
    private Curso cursoId;
    @JoinColumn(name = "PERSONA_ID", referencedColumnName = "ID")
    @ManyToOne
    private Persona personaId;
    @OneToMany(mappedBy = "alumnoId")
    private List<Deposito> depositoList;

    public Alumno() {
    }

    public Alumno(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    public Curso getCursoId() {
        return cursoId;
    }

    public void setCursoId(Curso cursoId) {
        this.cursoId = cursoId;
    }

    public Persona getPersonaId() {
        return personaId;
    }

    public void setPersonaId(Persona personaId) {
        this.personaId = personaId;
    }

    @XmlTransient
    public List<Deposito> getDepositoList() {
        return depositoList;
    }

    public void setDepositoList(List<Deposito> depositoList) {
        this.depositoList = depositoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alumno)) {
            return false;
        }
        Alumno other = (Alumno) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsurath.entity.Alumno[ id=" + id + " ]";
    }

    public BigInteger getEliminado() {
        return eliminado;
    }

    public void setEliminado(BigInteger eliminado) {
        this.eliminado = eliminado;
    }

}
