/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iformas
 */
@Entity
@Table(name = "CURSO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Curso.findAll", query = "SELECT c FROM Curso c")
    , @NamedQuery(name = "Curso.findById", query = "SELECT c FROM Curso c WHERE c.id = :id")
    , @NamedQuery(name = "Curso.findByNombre", query = "SELECT c FROM Curso c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Curso.findByColegio", query = "SELECT c FROM Curso c inner join c.colegioId co WHERE co.id = :id")
    , @NamedQuery(name = "Curso.findByEncargado", query = "SELECT c FROM Curso c inner join c.encargadoCurso ec WHERE ec.id = :id")
    , @NamedQuery(name = "Curso.findByContrato", query = "SELECT c FROM Curso c inner join c.contratoList cl inner join cl.cursoId c2  WHERE c2.id = :id")

})
public class Curso implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ANHO")
    private BigInteger anho;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ELIMINADO")
    private BigInteger eliminado;

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "secuencia_curso", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    private String nombre;
    @JoinColumn(name = "COLEGIO_ID", referencedColumnName = "ID")
    @ManyToOne
    private Colegio colegioId;
    @JoinColumn(name = "ENCARGADO_CURSO", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Usuario encargadoCurso;
    @OneToMany(mappedBy = "cursoId")
    private List<Alumno> alumnoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cursoId")
    private List<Contrato> contratoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cursoId")
    private List<Actividad> actividadList;

    public Curso() {
    }

    public Curso(BigDecimal id) {
        this.id = id;
    }

    public Curso(BigDecimal id, String nombre, BigInteger anho, BigInteger eliminado) {
        this.id = id;
        this.nombre = nombre;
        this.anho = anho;
        this.eliminado = eliminado;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Colegio getColegioId() {
        return colegioId;
    }

    public void setColegioId(Colegio colegioId) {
        this.colegioId = colegioId;
    }

    public Usuario getEncargadoCurso() {
        return encargadoCurso;
    }

    public void setEncargadoCurso(Usuario encargadoCurso) {
        this.encargadoCurso = encargadoCurso;
    }

    @XmlTransient
    public List<Alumno> getAlumnoList() {
        return alumnoList;
    }

    public void setAlumnoList(List<Alumno> alumnoList) {
        this.alumnoList = alumnoList;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @XmlTransient
    public List<Actividad> getActividadList() {
        return actividadList;
    }

    public void setActividadList(List<Actividad> actividadList) {
        this.actividadList = actividadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Curso)) {
            return false;
        }
        Curso other = (Curso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsurath.entity.Curso[ id=" + id + " ]";
    }

    public BigInteger getEliminado() {
        return eliminado;
    }

    public void setEliminado(BigInteger eliminado) {
        this.eliminado = eliminado;
    }

    public BigInteger getAnho() {
        return anho;
    }

    public void setAnho(BigInteger anho) {
        this.anho = anho;
    }

}
