/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author iformas
 */
@Entity
@Table(name = "METADATA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Metadata.findAll", query = "SELECT m FROM Metadata m")
    , @NamedQuery(name = "Metadata.findById", query = "SELECT m FROM Metadata m WHERE m.id = :id")
    , @NamedQuery(name = "Metadata.findByNombre", query = "SELECT m FROM Metadata m WHERE m.nombre = :nombre")
    , @NamedQuery(name = "Metadata.findByMime", query = "SELECT m FROM Metadata m WHERE m.mime = :mime")
    , @NamedQuery(name = "Metadata.findByHash", query = "SELECT m FROM Metadata m WHERE m.hash = :hash")
    , @NamedQuery(name = "Metadata.findByFecha", query = "SELECT m FROM Metadata m WHERE m.fecha = :fecha")})
public class Metadata implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "SEQ_GEN", sequenceName = "secuencia_metadata", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_GEN")
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "MIME")
    private String mime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "HASH")
    private String hash;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "metadataList")
    private Set<Contrato> contratoList = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "metadataList")
    private Set<Publicacion> publicacionList = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "metadataList")
    private Set<Respuesta> respuestaList = new HashSet<>();

//    @JoinTable(name = "ARCHIVOS_SERVICIO", joinColumns = {
//        @JoinColumn(name = "METADATA_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
//        @JoinColumn(name = "SERVICIO_ID", referencedColumnName = "ID")})
//    @ManyToMany
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "metadataList")
    private List<Servicio> servicioList;

    @JoinColumn(name = "ARCHIVO_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Archivo archivoId;

    public Metadata() {
    }

    public Metadata(BigDecimal id) {
        this.id = id;
    }

    public Metadata(BigDecimal id, String nombre, String mime, String hash, Date fecha) {
        this.id = id;
        this.nombre = nombre;
        this.mime = mime;
        this.hash = hash;
        this.fecha = fecha;
    }

    public Metadata(MetadataString elem) {
        this.archivoId = elem.getArchivoId();
        this.nombre = elem.getNombre();
        this.fecha = elem.getFecha();
        this.mime = elem.getMime();
        this.hash = elem.getHash();
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public Set<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(Set<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @XmlTransient
    public Set<Publicacion> getPublicacionList() {
        return publicacionList;
    }

    public void setPublicacionList(Set<Publicacion> publicacionList) {
        this.publicacionList = publicacionList;
    }

    @XmlTransient
    public Set<Respuesta> getRespuestaList() {
        return respuestaList;
    }

    public void setRespuestaList(Set<Respuesta> respuestaList) {
        this.respuestaList = respuestaList;
    }

    @XmlTransient
    public List<Servicio> getServicioList() {
        return servicioList;
    }

    public void setServicioList(List<Servicio> servicioList) {
        this.servicioList = servicioList;
    }

    public Archivo getArchivoId() {
        return archivoId;
    }

    public void setArchivoId(Archivo archivoId) {
        this.archivoId = archivoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Metadata)) {
            return false;
        }
        Metadata other = (Metadata) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tsurath.entity.Metadata[ id=" + id + " ]";
    }

}
