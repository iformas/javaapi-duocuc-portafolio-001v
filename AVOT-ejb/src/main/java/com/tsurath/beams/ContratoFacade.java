/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Contrato;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author iformas
 */
@Stateless
public class ContratoFacade extends AbstractFacade<Contrato> implements ContratoFacadeLocal {

    @PersistenceContext(unitName = "com.tsurath_AVOT-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ContratoFacade() {
        super(Contrato.class);
    }

    @Override
    public BigDecimal getMontoFaltante(BigDecimal idContrato) {
        return (BigDecimal) getEntityManager().createNamedQuery("Contrato.getMontoFaltante").setParameter("idContrato", idContrato).getSingleResult();
    }

    @Override
    public List<Contrato> listByCurso(BigDecimal id) {
        return getEntityManager().createNamedQuery("Contrato.findByCurso").setParameter("id", id).getResultList();
    }
    
    @Override
    public List<Contrato> findByEjecutivo(BigDecimal id){
        return getEntityManager().createNamedQuery("Contrato.findByEjecutivo").setParameter("id", id).getResultList();
    }
}
