/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Alumno;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author iformas
 */
@Stateless
public class AlumnoFacade extends AbstractFacade<Alumno> implements AlumnoFacadeLocal {

    @PersistenceContext(unitName = "com.tsurath_AVOT-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AlumnoFacade() {
        super(Alumno.class);
    }

    @Override
    public List<Alumno> ListPorApoderado(BigDecimal id) {
        return getEntityManager().createNamedQuery("Alumno.findByApoderado", Alumno.class).setParameter("id", id).getResultList();
    }

    @Override
    public List<Alumno> ListPorCurso(BigDecimal id) {
        return getEntityManager().createNamedQuery("Alumno.findByCurso", Alumno.class).setParameter("id", id).getResultList();
    }

    @Override
    public List<Alumno> ListPorColegio(BigDecimal id) {
        return getEntityManager().createNamedQuery("Alumno.findByColegio", Alumno.class).setParameter("id", id).getResultList();
    }

    @Override
    public Alumno findPorRutYCurso(BigInteger rut, char dv, BigDecimal cursoId) {
        return getEntityManager().createNamedQuery("Alumno.findByRutYCurso", Alumno.class).setParameter("rut", rut).setParameter("dv", dv).setParameter("cursoId", cursoId).getSingleResult();
    }

    @Override
    public BigDecimal SaldoAlumno(BigDecimal idContrato, BigDecimal idAlumno) {
        return (BigDecimal) getEntityManager().createNamedQuery("Alumno.getSaldoAlumno").setParameter("idContrato", idContrato).setParameter("idAlumno", idAlumno).getSingleResult();
        
    }

    @Override
    public BigDecimal MontoDepositadoAlumno(BigDecimal idContrato, BigDecimal idAlumno) {
        return (BigDecimal) getEntityManager().createNamedQuery("Alumno.getMontoDepositadoAlumno").setParameter("idContrato", idContrato).setParameter("idAlumno", idAlumno).getSingleResult();
    }

}
