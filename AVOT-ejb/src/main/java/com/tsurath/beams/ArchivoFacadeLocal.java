/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Archivo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author iformas
 */
@Local
public interface ArchivoFacadeLocal {

    Archivo create(Archivo archvo);

    void edit(Archivo archvo);

    void remove(Archivo archvo);

    Archivo find(Object id);

    List<Archivo> findAll();

    List<Archivo> findRange(int[] range);

    int count();
    
}
