/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Deposito;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author iformas
 */
@Local
public interface DepositoFacadeLocal {

    Deposito create(Deposito deposito);

    void edit(Deposito deposito);

    void remove(Deposito deposito);

    Deposito find(Object id);

    List<Deposito> findAll();

    List<Deposito> findByActividad(BigDecimal id);

    List<Deposito> findRange(int[] range);

    int getTotalRecaudado(int idContrato);

    int count();

}
