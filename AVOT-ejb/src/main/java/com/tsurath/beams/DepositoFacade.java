/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Deposito;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author iformas
 */
@Stateless
public class DepositoFacade extends AbstractFacade<Deposito> implements DepositoFacadeLocal {

    @PersistenceContext(unitName = "com.tsurath_AVOT-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DepositoFacade() {
        super(Deposito.class);
    }

    @Override
    public int getTotalRecaudado(int idContrato) {
        return (int) getEntityManager().createNamedQuery("Deposito.getTotalRecaudado").setParameter("idContrato", idContrato).getSingleResult();
    }

    @Override
    public List<Deposito> findByActividad(BigDecimal id) {
        return getEntityManager().createNamedQuery("Deposito.findByActividad", Deposito.class).setParameter("id", id).getResultList();
    }
    
}
