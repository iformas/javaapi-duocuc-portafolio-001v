/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Usuario;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author iformas
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "com.tsurath_AVOT-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }

    @Override
    public Usuario findByEmail(String email) {
        try {
            return getEntityManager().createNamedQuery("Usuario.findByEmail", Usuario.class).setParameter("email", email).getSingleResult();
        } catch (Exception ex) {
            throw ex;

        }
    }

    @Override
    public Usuario login(String email, String password) {
        try {
            Usuario elem = findByEmail(email);
            if (elem != null && elem.getPassword().equals(password)) {
                return elem;
            }
            return null;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public List<Usuario> ListApoderadoPorCurso(BigDecimal id) {
        return getEntityManager().createNamedQuery("Usuario.findApoderadosByCurso", Usuario.class).setParameter("id", id).getResultList();

    }

    @Override
    public Usuario findByRut(BigInteger rut) {
        return getEntityManager().createNamedQuery("Usuario.findByRut", Usuario.class).setParameter("rut", rut).getSingleResult();
    }
}
