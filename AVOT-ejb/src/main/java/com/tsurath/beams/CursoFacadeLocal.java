/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Curso;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author iformas
 */
@Local
public interface CursoFacadeLocal {

    Curso create(Curso curso);

    void edit(Curso curso);

    void remove(Curso curso);

    Curso find(Object id);

    List<Curso> findAll();
    
    List<Curso> listPorEncargado(BigDecimal id);

    List<Curso> listPorColegio(BigDecimal id);

    List<Curso> findRange(int[] range);

    Curso findByContrato(BigDecimal id);

    int count();

}
