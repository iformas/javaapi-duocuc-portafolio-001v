/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Metadata;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author iformas
 */
@Local
public interface MetadataFacadeLocal {

    Metadata create(Metadata metadata);

    void edit(Metadata metadata);

    void remove(Metadata metadata);

    Metadata find(Object id);

    List<Metadata> findAll();

    List<Metadata> findRange(int[] range);

    int count();
    
}
