/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Curso;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author iformas
 */
@Stateless
public class CursoFacade extends AbstractFacade<Curso> implements CursoFacadeLocal {

    @PersistenceContext(unitName = "com.tsurath_AVOT-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CursoFacade() {
        super(Curso.class);
    }

    @Override
    public List<Curso> listPorColegio(BigDecimal id) {
        return getEntityManager().createNamedQuery("Curso.findByColegio", Curso.class).setParameter("id", id).getResultList();
    }
    
    @Override
    public List<Curso> listPorEncargado(BigDecimal id) {
        return getEntityManager().createNamedQuery("Curso.findByEncargado", Curso.class).setParameter("id", id).getResultList();
    }

    @Override
    public Curso findByContrato(BigDecimal id) {
        return getEntityManager().createNamedQuery("Curso.findByContrato", Curso.class).setParameter("id", id).getSingleResult();
    }

}
