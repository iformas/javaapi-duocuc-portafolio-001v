/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.TipoActividad;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author iformas
 */
@Stateless
public class TipoActividadFacade extends AbstractFacade<TipoActividad> implements TipoActividadFacadeLocal {

    @PersistenceContext(unitName = "com.tsurath_AVOT-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoActividadFacade() {
        super(TipoActividad.class);
    }
    
}
