/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Curso;
import com.tsurath.entity.Usuario;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author iformas
 */
@Local
public interface UsuarioFacadeLocal {

    Usuario create(Usuario usuario);

    void edit(Usuario usuario);

    void remove(Usuario usuario);

    Usuario find(Object id);

    List<Usuario> findAll();

    List<Usuario> findRange(int[] range);

    List<Usuario> ListApoderadoPorCurso(BigDecimal id);

    Usuario findByEmail(String email);

    public Usuario login(String email, String password);

    int count();
    
    Usuario findByRut(BigInteger rut);

}
