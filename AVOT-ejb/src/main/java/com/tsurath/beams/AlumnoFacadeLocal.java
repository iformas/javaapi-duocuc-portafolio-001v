/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Alumno;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author iformas
 */
@Local
public interface AlumnoFacadeLocal {

    Alumno create(Alumno alumno);

    void edit(Alumno alumno);

    void remove(Alumno alumno);

    Alumno find(Object id);

    List<Alumno> findAll();

    List<Alumno> findRange(int[] range);

    Alumno findPorRutYCurso(BigInteger rut, char dv, BigDecimal cursoId);

    List<Alumno> ListPorApoderado(BigDecimal id);

    List<Alumno> ListPorCurso(BigDecimal id);

    List<Alumno> ListPorColegio(BigDecimal id);
    
    BigDecimal SaldoAlumno(BigDecimal idContrato, BigDecimal idAlumno);

    BigDecimal MontoDepositadoAlumno(BigDecimal idContrato, BigDecimal idAlumno);
    
    int count();

}
