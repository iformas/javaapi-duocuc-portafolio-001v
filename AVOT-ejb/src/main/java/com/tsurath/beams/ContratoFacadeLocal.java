/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsurath.beams;

import com.tsurath.entity.Contrato;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author iformas
 */
@Local
public interface ContratoFacadeLocal {

    Contrato create(Contrato contrato);

    void edit(Contrato contrato);

    void remove(Contrato contrato);

    Contrato find(Object id);

    List<Contrato> listByCurso(BigDecimal id);

    List<Contrato> findAll();

    List<Contrato> findRange(int[] range);

    BigDecimal getMontoFaltante(BigDecimal idContrato);
    
    List<Contrato> findByEjecutivo(BigDecimal id);

    int count();

}
